/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import com.mycompany.leadthefeedproject.enums.AgeGroup;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
@NamedEntityGraphs({
    @NamedEntityGraph(
            name = "subGroupWithAllergens",
            attributeNodes = {
                @NamedAttributeNode(value = "allergens"),
            }
    )
})
public class SubGroup {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long subGroupId;
    @ManyToOne (fetch = FetchType.EAGER)
    private MainGroup group;
    @NotNull
    private String subGroupName;
    @NotNull
    private AgeGroup ageGroup;
    @NotNull
    private int numberOfPersons;
    private double maxDailyEnergyKJ;
    private double maxDailyEnergyKcal;
    private double maxDailyProtein;
    private double maxDailyFat;
    private double maxDailySaturatedFat;
    private double maxDailyCarbohydrate;
    private double maxDailySugar;
    private int maxGlycemicIndexPerMeal;
    private double maxDailyFibre;
    private double maxDailyNatrium;
    private double maxDailyPotassium;
    private double maxDailyCalcium;
    private double maxDailyMagnesium;
    @ManyToMany
    private List<Allergen> allergens = new ArrayList<>();
    @OneToMany (mappedBy = "subGroup")
    private List<SubGroupRecepieHistory> recepieHistory = new ArrayList<>();
    @NotNull
    private LocalDateTime lastModified;
    @ManyToOne
    private UserEntity userOwned;

    public SubGroup() {
    }

    public SubGroup(MainGroup group, String subGroupName, AgeGroup ageGroup, int numberOfPersons, LocalDateTime lastModified, UserEntity userOwned) {
        this.group = group;
        this.subGroupName = subGroupName;
        this.ageGroup = ageGroup;
        this.numberOfPersons = numberOfPersons;
        this.lastModified = lastModified;
        this.userOwned = userOwned;
    }

    public SubGroup(String subGroupName, AgeGroup ageGroup, int numberOfPersons, double maxDailyEnergyKJ, double maxDailyEnergyKcal, double maxDailyProtein, double maxDailyFat, double maxDailySaturatedFat, double maxDailyCarbohydrate, double maxDailySugar, int maxGlycemicIndexPerMeal, double maxDailyFibre, double maxDailyNatrium, double maxDailyPotassium, double maxDailyCalcium, double maxDailyMagnesium, LocalDateTime lastModified, UserEntity userOwned) {
        this.subGroupName = subGroupName;
        this.ageGroup = ageGroup;
        this.numberOfPersons = numberOfPersons;
        this.maxDailyEnergyKJ = maxDailyEnergyKJ;
        this.maxDailyEnergyKcal = maxDailyEnergyKcal;
        this.maxDailyProtein = maxDailyProtein;
        this.maxDailyFat = maxDailyFat;
        this.maxDailySaturatedFat = maxDailySaturatedFat;
        this.maxDailyCarbohydrate = maxDailyCarbohydrate;
        this.maxDailySugar = maxDailySugar;
        this.maxGlycemicIndexPerMeal = maxGlycemicIndexPerMeal;
        this.maxDailyFibre = maxDailyFibre;
        this.maxDailyNatrium = maxDailyNatrium;
        this.maxDailyPotassium = maxDailyPotassium;
        this.maxDailyCalcium = maxDailyCalcium;
        this.maxDailyMagnesium = maxDailyMagnesium;
        this.lastModified = lastModified;
        this.userOwned = userOwned;
    }
    
    public long getSubGroupId() {
        return subGroupId;
    }

    public MainGroup getGroup() {
        return group;
    }

    public String getSubGroupName() {
        return subGroupName;
    }

    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public double getMaxDailyEnergyKJ() {
        return maxDailyEnergyKJ;
    }

    public double getMaxDailyEnergyKcal() {
        return maxDailyEnergyKcal;
    }

    public double getMaxDailyProtein() {
        return maxDailyProtein;
    }

    public double getMaxDailyFat() {
        return maxDailyFat;
    }

    public double getMaxDailySaturatedFat() {
        return maxDailySaturatedFat;
    }

    public double getMaxDailyCarbohydrate() {
        return maxDailyCarbohydrate;
    }

    public double getMaxDailySugar() {
        return maxDailySugar;
    }

    public int getMaxGlycemicIndexPerMeal() {
        return maxGlycemicIndexPerMeal;
    }

    public double getMaxDailyFibre() {
        return maxDailyFibre;
    }

    public double getMaxDailyNatrium() {
        return maxDailyNatrium;
    }

    public double getMaxDailyPotassium() {
        return maxDailyPotassium;
    }

    public double getMaxDailyCalcium() {
        return maxDailyCalcium;
    }

    public double getMaxDailyMagnesium() {
        return maxDailyMagnesium;
    }

    public List<Allergen> getAllergens() {
        return allergens;
    }

    public List<SubGroupRecepieHistory> getRecepieHistory() {
        return recepieHistory;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public UserEntity getUserOwned() {
        return userOwned;
    }

    public void setSubGroupId(long subGroupId) {
        this.subGroupId = subGroupId;
    }

    public void setGroup(MainGroup group) {
        this.group = group;
    }

    public void setSubGroupName(String subGroupName) {
        this.subGroupName = subGroupName;
    }

    public void setAgeGroup(AgeGroup ageGroup) {
        this.ageGroup = ageGroup;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public void setMaxDailyEnergyKJ(double maxDailyEnergyKJ) {
        this.maxDailyEnergyKJ = maxDailyEnergyKJ;
    }

    public void setMaxDailyEnergyKcal(double maxDailyEnergyKcal) {
        this.maxDailyEnergyKcal = maxDailyEnergyKcal;
    }

    public void setMaxDailyProtein(double maxDailyProtein) {
        this.maxDailyProtein = maxDailyProtein;
    }

    public void setMaxDailyFat(double maxDailyFat) {
        this.maxDailyFat = maxDailyFat;
    }

    public void setMaxDailySaturatedFat(double maxDailySaturatedFat) {
        this.maxDailySaturatedFat = maxDailySaturatedFat;
    }

    public void setMaxDailyCarbohydrate(double maxDailyCarbohydrate) {
        this.maxDailyCarbohydrate = maxDailyCarbohydrate;
    }

    public void setMaxDailySugar(double maxDailySugar) {
        this.maxDailySugar = maxDailySugar;
    }

    public void setMaxGlycemicIndexPerMeal(int maxGlycemicIndexPerMeal) {
        this.maxGlycemicIndexPerMeal = maxGlycemicIndexPerMeal;
    }

    public void setMaxDailyFibre(double maxDailyFibre) {
        this.maxDailyFibre = maxDailyFibre;
    }

    public void setMaxDailyNatrium(double maxDailyNatrium) {
        this.maxDailyNatrium = maxDailyNatrium;
    }

    public void setMaxDailyPotassium(double maxDailyPotassium) {
        this.maxDailyPotassium = maxDailyPotassium;
    }

    public void setMaxDailyCalcium(double maxDailyCalcium) {
        this.maxDailyCalcium = maxDailyCalcium;
    }

    public void setMaxDailyMagnesium(double maxDailyMagnesium) {
        this.maxDailyMagnesium = maxDailyMagnesium;
    }

    public void setAllergens(List<Allergen> allergens) {
        this.allergens = allergens;
    }

    public void setRecepieHistory(List<SubGroupRecepieHistory> recepieHistory) {
        this.recepieHistory = recepieHistory;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public void setUserOwned(UserEntity userOwned) {
        this.userOwned = userOwned;
    }
    
    

}
