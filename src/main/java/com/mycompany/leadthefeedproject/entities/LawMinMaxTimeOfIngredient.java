/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import com.mycompany.leadthefeedproject.enums.IngredientType;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
public class LawMinMaxTimeOfIngredient {

    @Id
    private IngredientType ingredientType;
    private int minTimePerDay;
    private int maxTimePerTenDay;

    public LawMinMaxTimeOfIngredient() {
    }

    public LawMinMaxTimeOfIngredient(IngredientType ingredientType, int minTimePerDay, int maxTimePerTenDay) {
        this.ingredientType = ingredientType;
        this.minTimePerDay = minTimePerDay;
        this.maxTimePerTenDay = maxTimePerTenDay;
    }
    
    public IngredientType getIngredientType() {
        return ingredientType;
    }

    public int getMinTimePerDay() {
        return minTimePerDay;
    }

    public int getMaxTimePerTenDay() {
        return maxTimePerTenDay;
    }

    public void setIngredientType(IngredientType ingredientType) {
        this.ingredientType = ingredientType;
    }

    public void setMinTimePerDay(int minTimePerDay) {
        this.minTimePerDay = minTimePerDay;
    }

    public void setMaxTimePerTenDay(int maxTimePerTenDay) {
        this.maxTimePerTenDay = maxTimePerTenDay;
    }
    
}
