/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import com.mycompany.leadthefeedproject.restcontroller.UserRestController;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author andras
 */
@Entity
public class UserEntity implements UserDetails {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String username;
    @NotNull
    private String emailAddress;
    @NotNull
    private String password;
    @ManyToMany(mappedBy = "roleUsers", fetch = FetchType.EAGER)
    private List<AuthorityRole> roles = new ArrayList();
    @OneToMany(mappedBy = "userOwned")
    private List<Menu> menuList = new ArrayList();
    @OneToMany(mappedBy = "userOwned")
    private List<MainGroup> groups = new ArrayList();

    public UserEntity() {
    }

    public UserEntity(String username, String emailAddress, List<AuthorityRole> roles) {
        LOGGER.debug("UserEntity constructor start");
        this.username = username;
        this.emailAddress = emailAddress;
        this.password = makeTemporaryPassord();
        this.roles = roles;
        LOGGER.debug("UserEntity constructor end");
    }

    public UserEntity(String username, String emailAddress, String password) {
        this.username = username;
        this.emailAddress = emailAddress;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public List<AuthorityRole> getRoles() {
        return roles;
    }

    public List<MainGroup> getGroups() {
        return groups;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(List<AuthorityRole> roles) {
        this.roles = roles;
    }

    public void setGroups(List<MainGroup> groups) {
        this.groups = groups;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() { //TODO
        return true;
    }

    private String makeTemporaryPassord() {
        byte[] array = new byte[8]; // length is bounded by 8
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));
        return generatedString;
    }
    
    public boolean isAdmin() {
        for (AuthorityRole role : roles) {
            if (role.getAuthority().equalsIgnoreCase("ADMIN")) {
                return true;
            }
        }
        return false;
    }
}
