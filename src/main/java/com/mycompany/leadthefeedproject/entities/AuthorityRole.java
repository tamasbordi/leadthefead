/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author andras
 */
@Entity
public class AuthorityRole implements GrantedAuthority {

    @Id
    private String roleName;
    @ManyToMany
    private List<UserEntity> roleUsers;
    
    private int hierarchy;

    public AuthorityRole() {
    }

    public AuthorityRole(String roleName, int hierarchy) {
        this.roleName = roleName;
        this.hierarchy = hierarchy;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<UserEntity> getRoleUsers() {
        return roleUsers;
    }

    public void setRoleUsers(List<UserEntity> roleUsers) {
        this.roleUsers = roleUsers;
    }

    public int getHierarchy() {
        return hierarchy;
    }

    public void setHierarchy(int hierarchy) {
        this.hierarchy = hierarchy;
    }
    
    @Override
    public String getAuthority() {
        return roleName;
    }
}
