/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
@NamedEntityGraphs({
    @NamedEntityGraph(
            name = "menuWithMeals",
            attributeNodes = {
                @NamedAttributeNode(value = "meals")
            }
    )
})
public class Menu {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long menuId;
    @NotNull
    private String menuName;
    @ManyToMany (mappedBy = "menusIncluding")
    private List<Meal> meals = new ArrayList<>();
    @NotNull
    private LocalDateTime lastModified;
    @ManyToOne
    private UserEntity userOwned;

    public Menu() {
    }

    public Menu(String menuName, LocalDateTime lastModified, UserEntity userOwned) {
        this.menuName = menuName;
        this.lastModified = lastModified;
        this.userOwned = userOwned;
    }
    
    public long getMenuId() {
        return menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public UserEntity getUserOwned() {
        return userOwned;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public void setUserOwned(UserEntity userOwned) {
        this.userOwned = userOwned;
    }
    
    

}
