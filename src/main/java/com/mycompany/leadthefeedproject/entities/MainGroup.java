/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
@NamedEntityGraph(
        name = "MainGroupsWithSubGroups",
        attributeNodes = {
                @NamedAttributeNode(value = "subGroup")
        }
)
public class MainGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long groupId;
    @NotNull
    private String groupName;
    @OneToMany(mappedBy = "group")
    private List<SubGroup> subGroup = new ArrayList<>();
    @NotNull
    private LocalDateTime lastModified;
    @ManyToOne
    private UserEntity userOwned;

    public MainGroup() {
    }

    public MainGroup(String groupName, LocalDateTime lastModified, UserEntity userOwned) {
        this.groupName = groupName;
        this.lastModified = lastModified;
        this.userOwned = userOwned;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public List<SubGroup> getSubGroup() {
        return subGroup;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public UserEntity getUserOwned() {
        return userOwned;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setSubGroup(List<SubGroup> subGroup) {
        this.subGroup = subGroup;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public void setUserOwned(UserEntity userOwned) {
        this.userOwned = userOwned;
    }

}
