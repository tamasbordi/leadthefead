/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
public class IngredientQuantityInRecepie {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long Id;
    @ManyToOne
    private Recepie recepie;
    @OneToOne 
    private IngredientHundredGram ingredient;
    @NotNull
    private double ingredientQuantityGram;
    @NotNull
    private double ingredientTotalKcal;

    public IngredientQuantityInRecepie() {
    }

    public IngredientQuantityInRecepie(Recepie recepie, IngredientHundredGram ingredient, double ingredientQuantityGram, double ingredientTotalKcal) {
        this.recepie = recepie;
        this.ingredient = ingredient;
        this.ingredientQuantityGram = ingredientQuantityGram;
        this.ingredientTotalKcal = ingredientTotalKcal;
    }
    
    public long getId() {
        return Id;
    }

    public Recepie getRecepie() {
        return recepie;
    }

    public IngredientHundredGram getIngredient() {
        return ingredient;
    }

    public double getIngredientQuantityGram() {
        return ingredientQuantityGram;
    }

    public void setId(long Id) {
        this.Id = Id;
    }

    public void setRecepie(Recepie recepie) {
        this.recepie = recepie;
    }

    public void setIngredient(IngredientHundredGram ingredient) {
        this.ingredient = ingredient;
    }

    public void setIngredientQuantityGram(double ingredientQuantityGram) {
        this.ingredientQuantityGram = ingredientQuantityGram;
    }

    public double getIngredientTotalKcal() {
        return ingredientTotalKcal;
    }

    public void setIngredientTotalKcal(double ingredientTotalKcal) {
        this.ingredientTotalKcal = ingredientTotalKcal;
    }
    
    
}
