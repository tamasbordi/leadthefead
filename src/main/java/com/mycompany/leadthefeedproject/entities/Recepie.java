/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
@NamedEntityGraphs({
    @NamedEntityGraph(
            name = "recepieWithMealsIncluding",
            attributeNodes = {
                @NamedAttributeNode(value = "mealsIncluding")
            }
    ),
    @NamedEntityGraph(
            name = "recepieWithIngredients",
            attributeNodes = {
                @NamedAttributeNode(value = "ingredients")
            }
    )
})
public class Recepie {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long recepieID;
    @NotNull
    private Boolean isDefault;
    @NotNull
    private String recepieName;
    @NotNull
    private String referencePerson;
    @OneToMany (mappedBy = "recepie")
    private List<IngredientQuantityInRecepie> ingredients = new ArrayList<>();
    @ManyToMany (mappedBy = "recepies")
    private List<Meal> mealsIncluding = new ArrayList<>();
    @NotNull
    private LocalDateTime lastModified;
    @ManyToOne
    private UserEntity userOwned;

    public Recepie() {
    }

    public Recepie(Boolean isDefault, String recepieName, String referencePerson, LocalDateTime lastModified, UserEntity userOwned) {
        this.isDefault = isDefault;
        this.recepieName = recepieName;
        this.referencePerson = referencePerson;
        this.lastModified = lastModified;
        this.userOwned = userOwned;
    }
    
    public long getRecepieID() {
        return recepieID;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public String getRecepieName() {
        return recepieName;
    }

    public String getReferencePerson() {
        return referencePerson;
    }

    public List<IngredientQuantityInRecepie> getIngredients() {
        return ingredients;
    }

    public List<Meal> getMealsIncluding() {
        return mealsIncluding;
    }
    
    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public UserEntity getUserOwned() {
        return userOwned;
    }

    public void setRecepieID(long recepieID) {
        this.recepieID = recepieID;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public void setRecepieName(String recepieName) {
        this.recepieName = recepieName;
    }

    public void setReferencePerson(String referencePerson) {
        this.referencePerson = referencePerson;
    }

    public void setIngredients(List<IngredientQuantityInRecepie> ingredients) {
        this.ingredients = ingredients;
    }

    public void setMealsIncluding(List<Meal> mealsIncluding) {
        this.mealsIncluding = mealsIncluding;
    }
    
    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public void setUserOwned(UserEntity userOwned) {
        this.userOwned = userOwned;
    }

}
