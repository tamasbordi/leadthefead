    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import com.mycompany.leadthefeedproject.enums.IngredientType;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
public class IngredientHundredGram {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long ingredientId;
    @NotNull
    private Boolean rawIngredient;
    @NotNull
    private IngredientType ingredientType;
    @NotNull
    private String ingredientName;
    @NotNull
    private double energyKJ;
    @NotNull
    private double energyKcal;
    @NotNull
    private double protein;
    @NotNull
    private double fat;
    @NotNull
    private double saturatedFat;
    @NotNull
    private double carbohydrate;
    @NotNull
    private double sugar;
    @NotNull
    private int glycemicIndex;
    @NotNull
    private double fibre;
    @NotNull
    private double natrium;
    @NotNull
    private double potassium;
    @NotNull
    private double calcium;
    @NotNull
    private double magnesium;
    @NotNull
    private Boolean allergenMilk;
    @NotNull
    private Boolean allergenGluten;
    @NotNull
    private Boolean allergenEgg;
    @NotNull
    private Boolean allergenShellfish;
    @NotNull
    private Boolean allergenFish;
    @NotNull
    private Boolean allergenMolluscs;
    @NotNull
    private Boolean allergenPeanut;
    @NotNull
    private Boolean allergenWalnut;
    private Boolean allergenSesame;
    @NotNull
    private Boolean allergenSoy;
    @NotNull
    private Boolean allergenCelery;
    @NotNull
    private Boolean allergenMustard;
    @NotNull
    private Boolean allergenSulpithes;
    @NotNull
    private Boolean allergenLupin;
    @NotNull
    private LocalDateTime lastModified;
    @ManyToOne
    private UserEntity lastModifier;

    public IngredientHundredGram() {
    }

    public IngredientHundredGram(Boolean rawIngredient, IngredientType ingredientType, String ingredientName, double energyKJ, double energyKcal, double protein, double fat, double saturatedFat, double carbohydrate, double sugar, int glycemicIndex, double fibre, double natrium, double potassium, double calcium, double magnesium, Boolean allergenMilk, Boolean allergenGluten, Boolean allergenEgg, Boolean allergenShellfish, Boolean allergenFish, Boolean allergenMolluscs, Boolean allergenPeanut, Boolean allergenWalnut, Boolean allergenSesame, Boolean allergenSoy, Boolean allergenCelery, Boolean allergenMustard, Boolean allergenSulpithes, Boolean allergenLupin, LocalDateTime lastModified, UserEntity lastModifier) {
        this.rawIngredient = rawIngredient;
        this.ingredientType = ingredientType;
        this.ingredientName = ingredientName;
        this.energyKJ = energyKJ;
        this.energyKcal = energyKcal;
        this.protein = protein;
        this.fat = fat;
        this.saturatedFat = saturatedFat;
        this.carbohydrate = carbohydrate;
        this.sugar = sugar;
        this.glycemicIndex = glycemicIndex;
        this.fibre = fibre;
        this.natrium = natrium;
        this.potassium = potassium;
        this.calcium = calcium;
        this.magnesium = magnesium;
        this.allergenMilk = allergenMilk;
        this.allergenGluten = allergenGluten;
        this.allergenEgg = allergenEgg;
        this.allergenShellfish = allergenShellfish;
        this.allergenFish = allergenFish;
        this.allergenMolluscs = allergenMolluscs;
        this.allergenPeanut = allergenPeanut;
        this.allergenWalnut = allergenWalnut;
        this.allergenSesame = allergenSesame;
        this.allergenSoy = allergenSoy;
        this.allergenCelery = allergenCelery;
        this.allergenMustard = allergenMustard;
        this.allergenSulpithes = allergenSulpithes;
        this.allergenLupin = allergenLupin;
        this.lastModified = lastModified;
        this.lastModifier = lastModifier;
    }
    

    public long getIngredientId() {
        return ingredientId;
    }

    public Boolean getRawIngredient() {
        return rawIngredient;
    }

    public IngredientType getIngredientType() {
        return ingredientType;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public double getEnergyKJ() {
        return energyKJ;
    }

    public double getEnergyKcal() {
        return energyKcal;
    }

    public double getProtein() {
        return protein;
    }

    public double getFat() {
        return fat;
    }

    public double getSaturatedFat() {
        return saturatedFat;
    }

    public double getCarbohydrate() {
        return carbohydrate;
    }

    public double getSugar() {
        return sugar;
    }

    public int getGlycemicIndex() {
        return glycemicIndex;
    }

    public double getFibre() {
        return fibre;
    }

    public double getNatrium() {
        return natrium;
    }

    public double getPotassium() {
        return potassium;
    }

    public double getCalcium() {
        return calcium;
    }

    public double getMagnesium() {
        return magnesium;
    }

    public Boolean getAllergenMilk() {
        return allergenMilk;
    }

    public Boolean getAllergenGluten() {
        return allergenGluten;
    }

    public Boolean getAllergenEgg() {
        return allergenEgg;
    }

    public Boolean getAllergenShellfish() {
        return allergenShellfish;
    }

    public Boolean getAllergenFish() {
        return allergenFish;
    }

    public Boolean getAllergenMolluscs() {
        return allergenMolluscs;
    }

    public Boolean getAllergenPeanut() {
        return allergenPeanut;
    }

    public Boolean getAllergenWalnut() {
        return allergenWalnut;
    }

    public Boolean getAllergenSesame() {
        return allergenSesame;
    }

    public Boolean getAllergenSoy() {
        return allergenSoy;
    }

    public Boolean getAllergenCelery() {
        return allergenCelery;
    }

    public Boolean getAllergenMustard() {
        return allergenMustard;
    }

    public Boolean getAllergenSulpithes() {
        return allergenSulpithes;
    }

    public Boolean getAllergenLupin() {
        return allergenLupin;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public UserEntity getLastModifier() {
        return lastModifier;
    }

    public void setIngredientId(long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public void setRawIngredient(Boolean rawIngredient) {
        this.rawIngredient = rawIngredient;
    }

    public void setIngredientType(IngredientType ingredientType) {
        this.ingredientType = ingredientType;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public void setEnergyKJ(double energyKJ) {
        this.energyKJ = energyKJ;
    }

    public void setEnergyKcal(double energyKcal) {
        this.energyKcal = energyKcal;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public void setSaturatedFat(double saturatedFat) {
        this.saturatedFat = saturatedFat;
    }

    public void setCarbohydrate(double carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public void setSugar(double sugar) {
        this.sugar = sugar;
    }

    public void setGlycemicIndex(int glycemicIndex) {
        this.glycemicIndex = glycemicIndex;
    }

    public void setFibre(double fibre) {
        this.fibre = fibre;
    }

    public void setNatrium(double natrium) {
        this.natrium = natrium;
    }

    public void setPotassium(double potassium) {
        this.potassium = potassium;
    }

    public void setCalcium(double calcium) {
        this.calcium = calcium;
    }

    public void setMagnesium(double magnesium) {
        this.magnesium = magnesium;
    }

    public void setAllergenMilk(Boolean allergenMilk) {
        this.allergenMilk = allergenMilk;
    }

    public void setAllergenGluten(Boolean allergenGluten) {
        this.allergenGluten = allergenGluten;
    }

    public void setAllergenEgg(Boolean allergenEgg) {
        this.allergenEgg = allergenEgg;
    }

    public void setAllergenShellfish(Boolean allergenShellfish) {
        this.allergenShellfish = allergenShellfish;
    }

    public void setAllergenFish(Boolean allergenFish) {
        this.allergenFish = allergenFish;
    }

    public void setAllergenMolluscs(Boolean allergenMolluscs) {
        this.allergenMolluscs = allergenMolluscs;
    }

    public void setAllergenPeanut(Boolean allergenPeanut) {
        this.allergenPeanut = allergenPeanut;
    }

    public void setAllergenWalnut(Boolean allergenWalnut) {
        this.allergenWalnut = allergenWalnut;
    }

    public void setAllergenSesame(Boolean allergenSesame) {
        this.allergenSesame = allergenSesame;
    }

    public void setAllergenSoy(Boolean allergenSoy) {
        this.allergenSoy = allergenSoy;
    }

    public void setAllergenCelery(Boolean allergenCelery) {
        this.allergenCelery = allergenCelery;
    }

    public void setAllergenMustard(Boolean allergenMustard) {
        this.allergenMustard = allergenMustard;
    }

    public void setAllergenSulpithes(Boolean allergenSulpithes) {
        this.allergenSulpithes = allergenSulpithes;
    }

    public void setAllergenLupin(Boolean allergenLupin) {
        this.allergenLupin = allergenLupin;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public void setLastModifier(UserEntity lastModifier) {
        this.lastModifier = lastModifier;
    }
    
    

}
