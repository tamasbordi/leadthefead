/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import com.mycompany.leadthefeedproject.enums.MealTime;
import com.mycompany.leadthefeedproject.enums.WeekDay;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
@NamedEntityGraphs({
    @NamedEntityGraph(
            name = "mealWithRecepies",
            attributeNodes = {
                @NamedAttributeNode(value = "recepies")
            }
    )
})
public class Meal {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long mealId;
    @NotNull
    private WeekDay day;
    @NotNull
    private MealTime mealTime;
    @ManyToMany
    private List<Recepie> recepies = new ArrayList<>();
    @ManyToMany 
    private List<Menu> menusIncluding = new ArrayList<>();
    @NotNull
    private LocalDateTime lastModified;
    @ManyToOne 
    private UserEntity userOwned;

    public Meal() {
    }

    public Meal(WeekDay day, MealTime mealTime, LocalDateTime lastModified, UserEntity userOwned) {
        this.day = day;
        this.mealTime = mealTime;
        this.lastModified = lastModified;
        this.userOwned = userOwned;
    }
    
    public long getMealId() {
        return mealId;
    }

    public WeekDay getDay() {
        return day;
    }

    public MealTime getMealTime() {
        return mealTime;
    }

    public List<Recepie> getRecepies() {
        return recepies;
    }

    public List<Menu> getMenusIncluding() {
        return menusIncluding;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public UserEntity getUserOwned() {
        return userOwned;
    }

    public void setMealId(long mealId) {
        this.mealId = mealId;
    }

    public void setDay(WeekDay day) {
        this.day = day;
    }

    public void setMealTime(MealTime mealTime) {
        this.mealTime = mealTime;
    }

    public void setRecepies(List<Recepie> recepies) {
        this.recepies = recepies;
    }

    public void setMenusIncluding(List<Menu> menusIncluding) {
        this.menusIncluding = menusIncluding;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public void setUserOwned(UserEntity userOwned) {
        this.userOwned = userOwned;
    }
    

}
