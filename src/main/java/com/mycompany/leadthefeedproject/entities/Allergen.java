/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
public class Allergen {
    
    @Id
    private String allergen;

    public Allergen() {
    }
    
    public Allergen(String allergen) {
        this.allergen = allergen;
    }

    public String getAllergen() {
        return allergen;
    }

    public void setAllergen(String allergen) {
        this.allergen = allergen;
    }
    
}
