/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import com.mycompany.leadthefeedproject.enums.AgeGroup;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
public class LawMinMaxPerDayPerAgeGroup {
    
    @Id
    private AgeGroup ageGroup;
    @NotNull
    private double minEnergyKcalPerPerson;
    @NotNull
    private double maxEnergyKcalPerPerson;
    @NotNull
    private double minNatriumPerPerson;
    @NotNull
    private double maxNatriumPerPerson;

    public LawMinMaxPerDayPerAgeGroup() {
    }

    public LawMinMaxPerDayPerAgeGroup(AgeGroup ageGroup, double minEnergyKcalPerPerson, double maxEnergyKcalPerPerson, double minNatriumPerPerson, double maxNatriumPerPerson) {
        this.ageGroup = ageGroup;
        this.minEnergyKcalPerPerson = minEnergyKcalPerPerson;
        this.maxEnergyKcalPerPerson = maxEnergyKcalPerPerson;
        this.minNatriumPerPerson = minNatriumPerPerson;
        this.maxNatriumPerPerson = maxNatriumPerPerson;
    }
    
    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public double getMinEnergyKcalPerPerson() {
        return minEnergyKcalPerPerson;
    }

    public double getMaxEnergyKcalPerPerson() {
        return maxEnergyKcalPerPerson;
    }

    public double getMinNatriumPerPerson() {
        return minNatriumPerPerson;
    }

    public double getMaxNatriumPerPerson() {
        return maxNatriumPerPerson;
    }

    public void setAgeGroup(AgeGroup ageGroup) {
        this.ageGroup = ageGroup;
    }

    public void setMinEnergyKcalPerPerson(double minEnergyKcalPerPerson) {
        this.minEnergyKcalPerPerson = minEnergyKcalPerPerson;
    }

    public void setMaxEnergyKcalPerPerson(double maxEnergyKcalPerPerson) {
        this.maxEnergyKcalPerPerson = maxEnergyKcalPerPerson;
    }

    public void setMinNatriumPerPerson(double minNatriumPerPerson) {
        this.minNatriumPerPerson = minNatriumPerPerson;
    }

    public void setMaxNatriumPerPerson(double maxNatriumPerPerson) {
        this.maxNatriumPerPerson = maxNatriumPerPerson;
    }
    
    
}
