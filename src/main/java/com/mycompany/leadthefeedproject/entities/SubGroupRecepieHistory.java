/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.entities;

import com.mycompany.leadthefeedproject.enums.AssigmentStatus;
import com.mycompany.leadthefeedproject.enums.IngredientType;
import com.mycompany.leadthefeedproject.enums.MealTime;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Entity
public class SubGroupRecepieHistory {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long subGroupRecepieHistoryId;
    @ManyToOne
    private SubGroup subGroup;
    @NotNull
    private AssigmentStatus status;
    @NotNull
    private String recepieName;
    @NotNull
    private MealTime mealTime;
    @NotNull
    private LocalDate date;
    @ManyToOne
    private UserEntity userOwned;
    @NotNull
    private IngredientType ingredientType;
    @NotNull
    private String ingredientName;
    @NotNull
    private double energyKJPerPerson;
    @NotNull
    private double energyKcalPerPerson;
    @NotNull
    private double proteinPerPerson;
    @NotNull
    private double fatPerPerson;
    @NotNull
    private double saturatedFatPerPerson;
    @NotNull
    private double carbohydratePerPerson;
    @NotNull
    private double sugarPerPerson;
    @NotNull
    private int glycemicIndexPerPerson;
    @NotNull
    private double fibrePerPerson;
    @NotNull
    private double natriumPerPerson;
    @NotNull
    private double potassiumPerPerson;
    @NotNull
    private double calciumPerPerson;
    @NotNull
    private double magnesiumPerPerson;
    @NotNull
    private Boolean allergenMilk;
    @NotNull
    private Boolean allergenGluten;
    @NotNull
    private Boolean allergenEgg;
    @NotNull
    private Boolean allergenShellfish;
    @NotNull
    private Boolean allergenFish;
    @NotNull
    private Boolean allergenMolluscs;
    @NotNull
    private Boolean allergenPeanut;
    @NotNull
    private Boolean allergenWalnut;
    @NotNull
    private Boolean allergenSesame;
    @NotNull
    private Boolean allergenSoy;
    @NotNull
    private Boolean allergenCelery;
    @NotNull
    private Boolean allergenMustard;
    @NotNull
    private Boolean allergenSulpithes;
    @NotNull
    private Boolean allergenLupin;

    public SubGroupRecepieHistory() {
    }

    public SubGroupRecepieHistory(SubGroup subGroup, AssigmentStatus status, String recepieName, MealTime mealTime, LocalDate date, UserEntity userOwned, IngredientType ingredientType, String ingredientName, double energyKJPerPerson, double energyKcalPerPerson, double proteinPerPerson, double fatPerPerson, double saturatedFatPerPerson, double carbohydratePerPerson, double sugarPerPerson, int glycemicIndexPerPerson, double fibrePerPerson, double natriumPerPerson, double potassiumPerPerson, double calciumPerPerson, double magnesiumPerPerson, Boolean allergenMilk, Boolean allergenGluten, Boolean allergenEgg, Boolean allergenShellfish, Boolean allergenFish, Boolean allergenMolluscs, Boolean allergenPeanut, Boolean allergenWalnut, Boolean allergenSesame, Boolean allergenSoy, Boolean allergenCelery, Boolean allergenMustard, Boolean allergenSulpithes, Boolean allergenLupin) {
        this.subGroup = subGroup;
        this.status = status;
        this.recepieName = recepieName;
        this.mealTime = mealTime;
        this.date = date;
        this.userOwned = userOwned;
        this.ingredientType = ingredientType;
        this.ingredientName = ingredientName;
        this.energyKJPerPerson = energyKJPerPerson;
        this.energyKcalPerPerson = energyKcalPerPerson;
        this.proteinPerPerson = proteinPerPerson;
        this.fatPerPerson = fatPerPerson;
        this.saturatedFatPerPerson = saturatedFatPerPerson;
        this.carbohydratePerPerson = carbohydratePerPerson;
        this.sugarPerPerson = sugarPerPerson;
        this.glycemicIndexPerPerson = glycemicIndexPerPerson;
        this.fibrePerPerson = fibrePerPerson;
        this.natriumPerPerson = natriumPerPerson;
        this.potassiumPerPerson = potassiumPerPerson;
        this.calciumPerPerson = calciumPerPerson;
        this.magnesiumPerPerson = magnesiumPerPerson;
        this.allergenMilk = allergenMilk;
        this.allergenGluten = allergenGluten;
        this.allergenEgg = allergenEgg;
        this.allergenShellfish = allergenShellfish;
        this.allergenFish = allergenFish;
        this.allergenMolluscs = allergenMolluscs;
        this.allergenPeanut = allergenPeanut;
        this.allergenWalnut = allergenWalnut;
        this.allergenSesame = allergenSesame;
        this.allergenSoy = allergenSoy;
        this.allergenCelery = allergenCelery;
        this.allergenMustard = allergenMustard;
        this.allergenSulpithes = allergenSulpithes;
        this.allergenLupin = allergenLupin;
    }
    
    public long getSubGroupRecepieHistoryId() {
        return subGroupRecepieHistoryId;
    }

    public SubGroup getSubGroup() {
        return subGroup;
    }

    public AssigmentStatus getStatus() {
        return status;
    }

    public String getRecepieName() {
        return recepieName;
    }

    public MealTime getMealTime() {
        return mealTime;
    }

    public LocalDate getDate() {
        return date;
    }

    public UserEntity getUserOwned() {
        return userOwned;
    }

    public IngredientType getIngredientType() {
        return ingredientType;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public double getEnergyKJPerPerson() {
        return energyKJPerPerson;
    }

    public double getEnergyKcalPerPerson() {
        return energyKcalPerPerson;
    }

    public double getProteinPerPerson() {
        return proteinPerPerson;
    }

    public double getFatPerPerson() {
        return fatPerPerson;
    }

    public double getSaturatedFatPerPerson() {
        return saturatedFatPerPerson;
    }

    public double getCarbohydratePerPerson() {
        return carbohydratePerPerson;
    }

    public double getSugarPerPerson() {
        return sugarPerPerson;
    }

    public int getGlycemicIndexPerPerson() {
        return glycemicIndexPerPerson;
    }

    public double getFibrePerPerson() {
        return fibrePerPerson;
    }

    public double getNatriumPerPerson() {
        return natriumPerPerson;
    }

    public double getPotassiumPerPerson() {
        return potassiumPerPerson;
    }

    public double getCalciumPerPerson() {
        return calciumPerPerson;
    }

    public double getMagnesiumPerPerson() {
        return magnesiumPerPerson;
    }

    public Boolean getAllergenMilk() {
        return allergenMilk;
    }

    public Boolean getAllergenGluten() {
        return allergenGluten;
    }

    public Boolean getAllergenEgg() {
        return allergenEgg;
    }

    public Boolean getAllergenShellfish() {
        return allergenShellfish;
    }

    public Boolean getAllergenFish() {
        return allergenFish;
    }

    public Boolean getAllergenMolluscs() {
        return allergenMolluscs;
    }

    public Boolean getAllergenPeanut() {
        return allergenPeanut;
    }

    public Boolean getAllergenWalnut() {
        return allergenWalnut;
    }

    public Boolean getAllergenSesame() {
        return allergenSesame;
    }

    public Boolean getAllergenSoy() {
        return allergenSoy;
    }

    public Boolean getAllergenCelery() {
        return allergenCelery;
    }

    public Boolean getAllergenMustard() {
        return allergenMustard;
    }

    public Boolean getAllergenSulpithes() {
        return allergenSulpithes;
    }

    public Boolean getAllergenLupin() {
        return allergenLupin;
    }

    public void setSubGroupRecepieHistoryId(long subGroupRecepieHistoryId) {
        this.subGroupRecepieHistoryId = subGroupRecepieHistoryId;
    }

    public void setSubGroup(SubGroup subGroup) {
        this.subGroup = subGroup;
    }

    public void setStatus(AssigmentStatus status) {
        this.status = status;
    }

    public void setRecepieName(String recepieName) {
        this.recepieName = recepieName;
    }

    public void setMealTime(MealTime mealTime) {
        this.mealTime = mealTime;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public void setUserOwned(UserEntity userOwned) {
        this.userOwned = userOwned;
    }

    public void setIngredientType(IngredientType ingredientType) {
        this.ingredientType = ingredientType;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public void setEnergyKJPerPerson(double energyKJPerPerson) {
        this.energyKJPerPerson = energyKJPerPerson;
    }

    public void setEnergyKcalPerPerson(double energyKcalPerPerson) {
        this.energyKcalPerPerson = energyKcalPerPerson;
    }

    public void setProteinPerPerson(double proteinPerPerson) {
        this.proteinPerPerson = proteinPerPerson;
    }

    public void setFatPerPerson(double fatPerPerson) {
        this.fatPerPerson = fatPerPerson;
    }

    public void setSaturatedFatPerPerson(double saturatedFatPerPerson) {
        this.saturatedFatPerPerson = saturatedFatPerPerson;
    }

    public void setCarbohydratePerPerson(double carbohydratePerPerson) {
        this.carbohydratePerPerson = carbohydratePerPerson;
    }

    public void setSugarPerPerson(double sugarPerPerson) {
        this.sugarPerPerson = sugarPerPerson;
    }

    public void setGlycemicIndexPerPerson(int glycemicIndexPerPerson) {
        this.glycemicIndexPerPerson = glycemicIndexPerPerson;
    }

    public void setFibrePerPerson(double fibrePerPerson) {
        this.fibrePerPerson = fibrePerPerson;
    }

    public void setNatriumPerPerson(double natriumPerPerson) {
        this.natriumPerPerson = natriumPerPerson;
    }

    public void setPotassiumPerPerson(double potassiumPerPerson) {
        this.potassiumPerPerson = potassiumPerPerson;
    }

    public void setCalciumPerPerson(double calciumPerPerson) {
        this.calciumPerPerson = calciumPerPerson;
    }

    public void setMagnesiumPerPerson(double magnesiumPerPerson) {
        this.magnesiumPerPerson = magnesiumPerPerson;
    }

    public void setAllergenMilk(Boolean allergenMilk) {
        this.allergenMilk = allergenMilk;
    }

    public void setAllergenGluten(Boolean allergenGluten) {
        this.allergenGluten = allergenGluten;
    }

    public void setAllergenEgg(Boolean allergenEgg) {
        this.allergenEgg = allergenEgg;
    }

    public void setAllergenShellfish(Boolean allergenShellfish) {
        this.allergenShellfish = allergenShellfish;
    }

    public void setAllergenFish(Boolean allergenFish) {
        this.allergenFish = allergenFish;
    }

    public void setAllergenMolluscs(Boolean allergenMolluscs) {
        this.allergenMolluscs = allergenMolluscs;
    }

    public void setAllergenPeanut(Boolean allergenPeanut) {
        this.allergenPeanut = allergenPeanut;
    }

    public void setAllergenWalnut(Boolean allergenWalnut) {
        this.allergenWalnut = allergenWalnut;
    }

    public void setAllergenSesame(Boolean allergenSesame) {
        this.allergenSesame = allergenSesame;
    }

    public void setAllergenSoy(Boolean allergenSoy) {
        this.allergenSoy = allergenSoy;
    }

    public void setAllergenCelery(Boolean allergenCelery) {
        this.allergenCelery = allergenCelery;
    }

    public void setAllergenMustard(Boolean allergenMustard) {
        this.allergenMustard = allergenMustard;
    }

    public void setAllergenSulpithes(Boolean allergenSulpithes) {
        this.allergenSulpithes = allergenSulpithes;
    }

    public void setAllergenLupin(Boolean allergenLupin) {
        this.allergenLupin = allergenLupin;
    }
    
}
