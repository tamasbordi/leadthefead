/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.restcontroller;

import com.mycompany.leadthefeedproject.dto.HttpErrorResponseDTO;
import com.mycompany.leadthefeedproject.dto.NewRecepieDTO;
import com.mycompany.leadthefeedproject.dto.RecepieDTO;
import com.mycompany.leadthefeedproject.dto.RecepieIngredientDTO;
import com.mycompany.leadthefeedproject.entities.IngredientHundredGram;
import com.mycompany.leadthefeedproject.entities.IngredientQuantityInRecepie;
import com.mycompany.leadthefeedproject.entities.Recepie;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.service.IngredientHundredGramService;
import com.mycompany.leadthefeedproject.service.IngredientQuantityInRecepieService;
import com.mycompany.leadthefeedproject.service.RecepieService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@RestController
public class RecepieRestController {

    private RecepieService dbConnectionRecepie;
    private IngredientQuantityInRecepieService dbConnectionIngredientQuantity;
    private IngredientHundredGramService dbConnectionIngredient;

    @Autowired
    public RecepieRestController(RecepieService dbConnectionRecepie, IngredientQuantityInRecepieService dbConnectionIngredientQuantity, IngredientHundredGramService dbConnectionIngredient) {
        this.dbConnectionRecepie = dbConnectionRecepie;
        this.dbConnectionIngredientQuantity = dbConnectionIngredientQuantity;
        this.dbConnectionIngredient = dbConnectionIngredient;
    }

    @RequestMapping(path = "recipes", method = RequestMethod.GET)
    public List<RecepieDTO> giveAllRecepiesWithIngredientsQuantity() {
        List<RecepieDTO> returnJson = new ArrayList<>();
        List<Recepie> allRecepie = dbConnectionRecepie.getAllRecepieWithIngredients();
        for (Recepie r : allRecepie) {
            long recepieID = r.getRecepieID();
            String recepieName = r.getRecepieName();
            String referencePerson = r.getReferencePerson();
            List<RecepieIngredientDTO> ingredients = new ArrayList<>();
            LocalDateTime lastModified = r.getLastModified();
            String userOwned = r.getUserOwned().getUsername();
            RecepieDTO recepieJson = new RecepieDTO(recepieID, recepieName, referencePerson, lastModified, userOwned, ingredients);
            List<IngredientQuantityInRecepie> ingredientsQuantity = r.getIngredients();
            for (IngredientQuantityInRecepie i : ingredientsQuantity) {
                long ingredientId = i.getIngredient().getIngredientId();
                double ingredientQuantity = i.getIngredientQuantityGram();
                recepieJson.getIngredients().add(new RecepieIngredientDTO(ingredientId, ingredientQuantity));
            }
            returnJson.add(recepieJson);
        }
        return returnJson;
    }

    @RequestMapping(path = "updaterecipe", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateRecepie(@RequestBody NewRecepieDTO changedRecepie) {
        HttpErrorResponseDTO errorList = new HttpErrorResponseDTO();
        //Validations
        errorList.getErrors().addAll(validateRecepieId(changedRecepie.getRecepieID()));
        errorList.getErrors().addAll(validateRecepieNameAndReferencePerson(changedRecepie.getRecepieName(), changedRecepie.getReferencePerson(), changedRecepie.getRecepieID()));
        errorList.getErrors().addAll(validateRecepieIngredients(changedRecepie.getIngredients()));
        if (!errorList.getErrors().isEmpty()) {
            return new ResponseEntity(errorList, HttpStatus.BAD_REQUEST);
        }
        //Update database
        dbConnectionRecepie.updateRecepie(changedRecepie.getRecepieID(), changedRecepie.getRecepieName(), changedRecepie.getReferencePerson(), changedRecepie.getIngredients());
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(path = "newrecipe", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveNewRecepie(@RequestBody NewRecepieDTO newRecepie) {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        HttpErrorResponseDTO errorList = new HttpErrorResponseDTO();
        //Validations
        errorList.getErrors().addAll(validateRecepieNameAndReferencePerson(newRecepie.getRecepieName(), newRecepie.getReferencePerson()));
        errorList.getErrors().addAll(validateRecepieIngredients(newRecepie.getIngredients()));
        if (!errorList.getErrors().isEmpty()) {
            return new ResponseEntity(errorList, HttpStatus.BAD_REQUEST);
        }
        //Insert into database
        Recepie recepie = new Recepie(user.isAdmin(), newRecepie.getRecepieName(), newRecepie.getReferencePerson(), LocalDateTime.now(), user);
        dbConnectionRecepie.addNewRecepie(recepie);
        List<RecepieIngredientDTO> responseIngredientQuantityList = new ArrayList<>();
        for (RecepieIngredientDTO ingredient : newRecepie.getIngredients()) {
            IngredientHundredGram i = dbConnectionIngredient.findIngredientById(ingredient.getIngredientId());
            IngredientQuantityInRecepie ingredientQuantity = new IngredientQuantityInRecepie(recepie, i, ingredient.getIngredientQuantity(), ingredient.getIngredientQuantity() * (i.getEnergyKcal() / 100));
            responseIngredientQuantityList.add(new RecepieIngredientDTO(ingredient.getIngredientId(), ingredient.getIngredientQuantity()));
            dbConnectionIngredientQuantity.addIngredientQuantityInRecepie(ingredientQuantity);
        }
        RecepieDTO recepieDTO = new RecepieDTO(recepie.getRecepieID(), recepie.getRecepieName(), recepie.getReferencePerson(), recepie.getLastModified(), recepie.getUserOwned().getUsername(), responseIngredientQuantityList);
        return new ResponseEntity(recepieDTO, HttpStatus.OK);
    }

    private List<String> validateRecepieId(long id) {
        List<String> errors = new ArrayList<>();
        try {
            dbConnectionRecepie.findRecepieByID(id);
        } catch (NoResultException ex) {
            errors.add("no such id: " + id);
        }
        return errors;
    }

    private List<String> validateRecepieNameAndReferencePerson(String name, String referencePerson, long id) {
        List<String> errors = new ArrayList<>();
        if (name.isEmpty()) {
            errors.add("recepie name is empty: " + id);
        }
        if (referencePerson.isEmpty()) {
            errors.add("reference person is empty: " + id);
        }
        return errors;
    }

    private List<String> validateRecepieNameAndReferencePerson(String name, String referencePerson) {
        List<String> errors = new ArrayList<>();
        if (name.isEmpty()) {
            errors.add("recepie name is empty");
        }
        if (referencePerson.isEmpty()) {
            errors.add("reference person is empty:");
        }
        return errors;
    }

    private List<String> validateRecepieIngredients(List<RecepieIngredientDTO> ingredients) {
        List<String> errors = new ArrayList<>();
        for (RecepieIngredientDTO i : ingredients) {
            if (i.getIngredientQuantity() < 0) {
                errors.add("ingredient quantity is less than zero: " + i.getIngredientId());
            }
        }
        return errors;
    }
}
