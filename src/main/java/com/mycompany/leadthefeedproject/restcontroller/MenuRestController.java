/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.restcontroller;

import com.mycompany.leadthefeedproject.dto.HttpErrorResponseDTO;
import com.mycompany.leadthefeedproject.dto.MenuDetailDayDTO;
import com.mycompany.leadthefeedproject.dto.MenuDetailsDTO;
import com.mycompany.leadthefeedproject.dto.MenuListDTO;
import com.mycompany.leadthefeedproject.dto.NewRecepieDTO;
import com.mycompany.leadthefeedproject.entities.Meal;
import com.mycompany.leadthefeedproject.entities.Menu;
import com.mycompany.leadthefeedproject.entities.Recepie;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.enums.MealTime;
import com.mycompany.leadthefeedproject.enums.WeekDay;
import com.mycompany.leadthefeedproject.service.MealService;
import com.mycompany.leadthefeedproject.service.MenuService;
import com.mycompany.leadthefeedproject.service.RecepieService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@RestController
public class MenuRestController {

    private MenuService dbConnectionMenu;
    private MealService dbConnectionMeal;
    private RecepieService dbConnectionRecepie;

    @Autowired
    public MenuRestController(MenuService dbConnectionMenu, MealService dbConnectionMeal, RecepieService dbConnectionRecepie) {
        this.dbConnectionMenu = dbConnectionMenu;
        this.dbConnectionMeal = dbConnectionMeal;
        this.dbConnectionRecepie = dbConnectionRecepie;
    }

    @RequestMapping(path = "menusofuser", method = RequestMethod.GET)
    public List<MenuListDTO> giveListOfMenusOfUser() {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = user.getId();

        List<MenuListDTO> returnJson = new ArrayList<>();

        List<Menu> menusOfUser = dbConnectionMenu.getAllTheMenusOfUser(userId);

        for (Menu menu : menusOfUser) {
            Set<WeekDay> daysCovered = new HashSet<>();
            for (Meal meal : menu.getMeals()) {
                if (! dbConnectionMeal.getMealWithRecepies(meal.getMealId()).getRecepies().isEmpty()) {
                    daysCovered.add(meal.getDay());
                }
            }
            MenuListDTO menuListDTO = new MenuListDTO(menu.getMenuId(), menu.getMenuName(), daysCovered.size(), menu.getLastModified(), user.getUsername());
            returnJson.add(menuListDTO);
        }
        
        return returnJson;
    }

    @RequestMapping(path = "menudetails", method = RequestMethod.GET)
    public MenuDetailsDTO giveMenuDetails(@RequestParam("menuId") Long menuId) {
        //TODO menu owner check
        HashMap<WeekDay, MenuDetailDayDTO> weekDays = new HashMap<>();

        Menu menu = dbConnectionMenu.getMenuWithMeals(menuId);
        List<Meal> meals = menu.getMeals();
        for (Meal m : meals) {
            Meal meal = dbConnectionMeal.findMealWithRecepies(m.getMealId());
            WeekDay day = meal.getDay();
            MenuDetailDayDTO menuDay;
            if (weekDays.containsKey(day)) {
                menuDay = weekDays.get(day);
            } else {
                menuDay = new MenuDetailDayDTO();
                weekDays.put(day, menuDay);
            }
            List<Recepie> recepies = meal.getRecepies();
            HashSet<Long> recepieIDs = new HashSet<>();
            for (Recepie r : recepies) {
                switch (m.getMealTime()) {
                    case BREAKFAST:
                        menuDay.getBreakfastRecipeIDs().add(r.getRecepieID());
                        break;
                    case FORENOON_SNACK:
                        menuDay.getForenoonSnackRecipeIDs().add(r.getRecepieID());
                        break;
                    case LUNCH:
                        menuDay.getLunchRecipeIDs().add(r.getRecepieID());
                        break;
                    case AFTERNOON_SNACK:
                        menuDay.getAfternoonSnackRecipeIDs().add(r.getRecepieID());
                        break;
                    case DINNER:
                        menuDay.getDinnerSnackRecipeIDs().add(r.getRecepieID());
                        break;
                }
            }
        }
        return new MenuDetailsDTO(menu.getMenuId(), menu.getMenuName(), weekDays, menu.getLastModified(), menu.getUserOwned().getUsername());
    }

    @RequestMapping(path = "updatemenu", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateMenu(@RequestBody MenuDetailsDTO menuDTO) {
        HttpErrorResponseDTO errorList = new HttpErrorResponseDTO();
        Menu menu = dbConnectionMenu.getMenuWithMeals(menuDTO.getMenuId());

        //Validations
        errorList.getErrors().addAll(validateMenuName(menuDTO.getMenuName(), menuDTO.getMenuId()));
        if (!errorList.getErrors().isEmpty()) {
            return new ResponseEntity(errorList, HttpStatus.BAD_REQUEST);
        }

        //Update DB
        dbConnectionMenu.updateMenuName(menuDTO.getMenuId(), menuDTO.getMenuName());
        List<Long> mealsToDelete = new ArrayList<>();
        for (Meal meal : menu.getMeals()) {
            dbConnectionMeal.deleteMealFromMenu(meal, menu);;
        }
        HashMap<WeekDay, MenuDetailDayDTO> weekDays = menuDTO.getWeekDays();
        List<Meal> newMeals = new ArrayList<>();
        for (WeekDay weekDay : weekDays.keySet()) {
            newMeals.addAll(createNewMealsForMenuUpdateOrSave(weekDay, weekDays.get(weekDay)));
        }
        for (Meal newMeal : newMeals) {
            dbConnectionMeal.addMealToMenu(newMeal, menu);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(path = "newmenu", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveNewMenu(@RequestBody MenuDetailsDTO menuDTO) {
        HttpErrorResponseDTO errorList = new HttpErrorResponseDTO();
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //Validations
        errorList.getErrors().addAll(validateMenuName(menuDTO.getMenuName(), menuDTO.getMenuId()));
        if (!errorList.getErrors().isEmpty()) {
            return new ResponseEntity(errorList, HttpStatus.BAD_REQUEST);
        }
        Menu menu = new Menu(menuDTO.getMenuName(), LocalDateTime.now(), user);
        dbConnectionMenu.addNewMenu(menu);
        HashMap<WeekDay, MenuDetailDayDTO> weekDays = menuDTO.getWeekDays();
        List<Meal> newMeals = new ArrayList<>();
        for (WeekDay weekDay : weekDays.keySet()) {
            newMeals.addAll(createNewMealsForMenuUpdateOrSave(weekDay, weekDays.get(weekDay)));
        }
        for (Meal newMeal : newMeals) {
            dbConnectionMeal.addMealToMenu(newMeal, menu);
        }
        MenuDetailsDTO giveMenuDetails = giveMenuDetails(menu.getMenuId());
        return new ResponseEntity(giveMenuDetails, HttpStatus.OK);
    }

    private List<String> validateMenuName(String name, long id) {
        List<String> errors = new ArrayList<>();
        if (name.isEmpty()) {
            errors.add("menu name is empty: " + id);
        }
        return errors;
    }

    private List<Meal> createNewMealsForMenuUpdateOrSave(WeekDay day, MenuDetailDayDTO dto) {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Meal> newMeals = new ArrayList<>();
        //MONDAY
        List<Long> breakfast = dto.getBreakfastRecipeIDs();
        Meal breakfastMeal = new Meal(day, MealTime.BREAKFAST, LocalDateTime.now(), user);
        dbConnectionMeal.addNewMeal(breakfastMeal);
        newMeals.add(breakfastMeal);
        for (Long id : breakfast) {
            Recepie recepie = dbConnectionRecepie.findRecepieByID(id);
            dbConnectionMeal.addRecepieToMeal(breakfastMeal, recepie);
        }
        List<Long> forenoonSnack = dto.getForenoonSnackRecipeIDs();
        Meal fSnackMeal = new Meal(day, MealTime.FORENOON_SNACK, LocalDateTime.now(), user);
        dbConnectionMeal.addNewMeal(fSnackMeal);
        newMeals.add(fSnackMeal);
        for (Long id : forenoonSnack) {
            Recepie recepie = dbConnectionRecepie.findRecepieByID(id);
            dbConnectionMeal.addRecepieToMeal(fSnackMeal, recepie);
        }
        List<Long> lunch = dto.getLunchRecipeIDs();
        Meal lunchMeal = new Meal(day, MealTime.LUNCH, LocalDateTime.now(), user);
        dbConnectionMeal.addNewMeal(lunchMeal);
        newMeals.add(lunchMeal);
        for (Long id : lunch) {
            Recepie recepie = dbConnectionRecepie.findRecepieByID(id);
            dbConnectionMeal.addRecepieToMeal(lunchMeal, recepie);
        }
        List<Long> afternoonSnack = dto.getAfternoonSnackRecipeIDs();
        Meal aSnackMeal = new Meal(day, MealTime.AFTERNOON_SNACK, LocalDateTime.now(), user);
        dbConnectionMeal.addNewMeal(aSnackMeal);
        newMeals.add(aSnackMeal);
        for (Long id : afternoonSnack) {
            Recepie recepie = dbConnectionRecepie.findRecepieByID(id);
            dbConnectionMeal.addRecepieToMeal(aSnackMeal, recepie);
        }
        List<Long> dinner = dto.getDinnerSnackRecipeIDs();
        Meal dinnerMeal = new Meal(day, MealTime.DINNER, LocalDateTime.now(), user);
        dbConnectionMeal.addNewMeal(dinnerMeal);
        newMeals.add(dinnerMeal);
        for (Long id : dinner) {
            Recepie recepie = dbConnectionRecepie.findRecepieByID(id);
            dbConnectionMeal.addRecepieToMeal(dinnerMeal, recepie);
        }
        return newMeals;
    }
}
