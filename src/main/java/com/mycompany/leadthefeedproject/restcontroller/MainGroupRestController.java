/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.restcontroller;

import com.mycompany.leadthefeedproject.dto.GroupsOfUserDTO;
import com.mycompany.leadthefeedproject.dto.HttpErrorResponseDTO;
import com.mycompany.leadthefeedproject.dto.NewGroupDTO;
import com.mycompany.leadthefeedproject.entities.Allergen;
import com.mycompany.leadthefeedproject.entities.MainGroup;
import com.mycompany.leadthefeedproject.entities.SubGroup;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.enums.AgeGroup;
import com.mycompany.leadthefeedproject.service.MainGroupService;
import com.mycompany.leadthefeedproject.service.SubGroupService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@RestController
public class MainGroupRestController {

    private MainGroupService dbConnectionMain;
    private SubGroupService dbConnectionSub;

    @Autowired
    public MainGroupRestController(MainGroupService dbConnectionMain, SubGroupService dbConnectionSub) {
        this.dbConnectionMain = dbConnectionMain;
        this.dbConnectionSub = dbConnectionSub;
    }

    @RequestMapping(path = "groupsofuser", method = RequestMethod.GET)
    public List<GroupsOfUserDTO> giveAllGroupWithSubGroupOfUser() {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = user.getId();

        List<GroupsOfUserDTO> returnJson = new ArrayList<>();
        List<SubGroup> groupsOfUser = dbConnectionSub.getSubGroupsOfUserWithMainGroupsAndAllergens(userId);
        for (SubGroup sg : groupsOfUser) {
            AgeGroup ageGroup = sg.getAgeGroup();
            List<Allergen> allergens = sg.getAllergens();
            Long mainGroupId = sg.getGroup().getGroupId();
            String mainGroupName = sg.getGroup().getGroupName();
            LocalDateTime lastModified = sg.getLastModified();
            double maxDailyCalcium = sg.getMaxDailyCalcium();
            double maxDailyCarbohydrate = sg.getMaxDailyCarbohydrate();
            double maxDailyEnergyKJ = sg.getMaxDailyEnergyKJ();
            double maxDailyEnergyKcal = sg.getMaxDailyEnergyKcal();
            double maxDailyFat = sg.getMaxDailyFat();
            double maxDailyFibre = sg.getMaxDailyFibre();
            double maxDailyMagnesium = sg.getMaxDailyMagnesium();
            double maxDailyNatrium = sg.getMaxDailyNatrium();
            double maxDailyPotassium = sg.getMaxDailyPotassium();
            double maxDailyProtein = sg.getMaxDailyProtein();
            double maxDailySaturatedFat = sg.getMaxDailySaturatedFat();
            double maxDailySugar = sg.getMaxDailySugar();
            int maxGlycemicIndexPerMeal = sg.getMaxGlycemicIndexPerMeal();
            int numberOfPersons = sg.getNumberOfPersons();
            long subGroupId = sg.getSubGroupId();
            String subGroupName = sg.getSubGroupName();
            GroupsOfUserDTO dto = new GroupsOfUserDTO(mainGroupId, mainGroupName, subGroupId, subGroupName, ageGroup, numberOfPersons,
                    maxDailyEnergyKJ, maxDailyEnergyKcal, maxDailyProtein, maxDailyFat, maxDailySaturatedFat,
                    maxDailyCarbohydrate, maxDailySugar, maxGlycemicIndexPerMeal, maxDailyFibre, maxDailyNatrium,
                    maxDailyPotassium, maxDailyCalcium, maxDailyCalcium, lastModified, allergens);
            returnJson.add(dto);
        }
        return returnJson;
    }

    @RequestMapping(path = "newsubgroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveNewSubGroupList(@RequestBody List<NewGroupDTO> newGroups) {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //Check MainGroup exists
        MainGroup mainGroup;
        try {
            mainGroup = dbConnectionMain.findMainGroupOfUserByName(newGroups.get(0).getGroupName(), user.getId());
        } catch (NoResultException ex) {
            mainGroup = new MainGroup(newGroups.get(0).getGroupName(), LocalDateTime.now(), user);
            dbConnectionMain.addNewMainGroup(mainGroup);
            mainGroup = dbConnectionMain.findMainGroupOfUserByName(newGroups.get(0).getGroupName(), user.getId());
        } 
        //Validate input
        HttpErrorResponseDTO errorList = new HttpErrorResponseDTO();
        List<String> errors = errorList.getErrors();
        for (NewGroupDTO newGroup : newGroups) {
            errors.addAll(validateSubGroupName(newGroup.getSubGroupName(), user));
            errors.addAll(validateAgeGroup(newGroup.getAgeGroup(), newGroup.getGroupName()));
            errors.addAll(validateNumberOfPersons(newGroup.getNumberOfPersons(), newGroup.getSubGroupName()));
        }
        if (!errors.isEmpty()) {
            return new ResponseEntity(errorList, HttpStatus.BAD_REQUEST);
        }
        //Insert into DB
        for (NewGroupDTO newGroup : newGroups) {
            SubGroup subGroup = new SubGroup(newGroup.getSubGroupName(), newGroup.getAgeGroup(), newGroup.getNumberOfPersons(), newGroup.getMaxDailyEnergyKJ(), newGroup.getMaxDailyEnergyKcal(),
                     newGroup.getMaxDailyProtein(), newGroup.getMaxDailyFat(), newGroup.getMaxDailySaturatedFat(), newGroup.getMaxDailyCarbohydrate(),
                     newGroup.getMaxDailySugar(), newGroup.getMaxGlycemicIndexPerMeal(), newGroup.getMaxDailyFibre(), newGroup.getMaxDailyNatrium(),
                     newGroup.getMaxDailyPotassium(), newGroup.getMaxDailyCalcium(), newGroup.getMaxDailyMagnesium(), LocalDateTime.now(), user);
            dbConnectionSub.addNewSubGroup(subGroup);
            dbConnectionSub.addAllergensToSubGroup(subGroup, newGroup.getAllergens());
            dbConnectionSub.addSubGroupToMainGroup(subGroup, mainGroup);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(path = "updatesubgroup", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateSubGroupList(@RequestBody List<NewGroupDTO> newGroups) {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        long mainGroupId = 0;
        List<NewGroupDTO> toUpdate = new ArrayList<>();
        List<NewGroupDTO> toCreate = new ArrayList<>();
        for (NewGroupDTO newGroup : newGroups) {
            if (newGroup.getMainGroupId() != null) {
                mainGroupId = newGroup.getMainGroupId();
            }
            if (newGroup.getSubGroupId() != null) {
                toUpdate.add(newGroup);
            } else {
                toCreate.add(newGroup);
            }
        }
        //Validate input
        HttpErrorResponseDTO errorList = new HttpErrorResponseDTO();
        List<String> errors = errorList.getErrors();
         for (NewGroupDTO newGroup : newGroups) {
            errors.addAll(validateAgeGroup(newGroup.getAgeGroup(), newGroup.getGroupName()));
            errors.addAll(validateNumberOfPersons(newGroup.getNumberOfPersons(), newGroup.getSubGroupName()));
        }
         for (NewGroupDTO newGroup : toCreate) {
            errors.addAll(validateSubGroupNameForUpdate(newGroup.getSubGroupName(), newGroup.getSubGroupId(), user));
        }
        if (!errors.isEmpty()) {
            return new ResponseEntity(errorList, HttpStatus.BAD_REQUEST);
        }
        //Update in DB
        
        MainGroup mainGroup = dbConnectionMain.findMainGroupById(mainGroupId);
        for (NewGroupDTO newGroup : toUpdate) {
            dbConnectionSub.updateSubGroup(newGroup.getSubGroupId(), newGroup.getSubGroupName(), newGroup.getAgeGroup()
                    , newGroup.getNumberOfPersons(),newGroup.getMaxDailyEnergyKJ(), newGroup.getMaxDailyEnergyKcal(),
                     newGroup.getMaxDailyProtein(), newGroup.getMaxDailyFat(), newGroup.getMaxDailySaturatedFat(), newGroup.getMaxDailyCarbohydrate(),
                     newGroup.getMaxDailySugar(), newGroup.getMaxGlycemicIndexPerMeal(), newGroup.getMaxDailyFibre(), newGroup.getMaxDailyNatrium(),
                     newGroup.getMaxDailyPotassium(), newGroup.getMaxDailyCalcium(), newGroup.getMaxDailyMagnesium(), LocalDateTime.now(), newGroup.getAllergens());
        }
        for (NewGroupDTO newGroup : toCreate) {
            SubGroup subGroup = new SubGroup(newGroup.getSubGroupName(), newGroup.getAgeGroup(), newGroup.getNumberOfPersons(), newGroup.getMaxDailyEnergyKJ(), newGroup.getMaxDailyEnergyKcal(),
                     newGroup.getMaxDailyProtein(), newGroup.getMaxDailyFat(), newGroup.getMaxDailySaturatedFat(), newGroup.getMaxDailyCarbohydrate(),
                     newGroup.getMaxDailySugar(), newGroup.getMaxGlycemicIndexPerMeal(), newGroup.getMaxDailyFibre(), newGroup.getMaxDailyNatrium(),
                     newGroup.getMaxDailyPotassium(), newGroup.getMaxDailyCalcium(), newGroup.getMaxDailyMagnesium(), LocalDateTime.now(), user);
            dbConnectionSub.addNewSubGroup(subGroup);
            dbConnectionSub.addAllergensToSubGroup(subGroup, newGroup.getAllergens());
            dbConnectionSub.addSubGroupToMainGroup(subGroup, mainGroup);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    private List<String> validateSubGroupName(String name, UserEntity user) {
        //If already exist or empty
        List<String> errors = new ArrayList<>();
        if (name.isEmpty()) {
            errors.add("sub group name is empty");
            return errors;
        }
        try {
            dbConnectionSub.findSubGroupOfUserByName(name, user.getId());
            errors.add("sub group name already exists: " + name);
        } catch (NoResultException ex) {
        }
        return errors;
    }
    
    private List<String> validateSubGroupNameForUpdate(String name, Long subGroupId, UserEntity user) {
        //If already exist or empty
        List<String> errors = new ArrayList<>();
        if (name.isEmpty()) {
            errors.add("sub group name is empty");
            return errors;
        }
        try {
            SubGroup sg = dbConnectionSub.findSubGroupOfUserByName(name, user.getId());
            if (subGroupId != sg.getSubGroupId()) {
                errors.add("sub group name already exists: " + name);
            }
        } catch (NoResultException ex) {
        } catch (NullPointerException ex) {
        }
        return errors;
    }

    private List<String> validateAgeGroup(AgeGroup ageGroup, String name) {
        //If empty
        List<String> errors = new ArrayList<>();
        if (ageGroup == null) {
            errors.add("age group name is empty: " + name);
        }
        return errors;
    }

    private List<String> validateNumberOfPersons(int numberOfPersons, String name) {
        //If less than 0
        List<String> errors = new ArrayList<>();
        if (numberOfPersons < 0) {
            errors.add("number of persons is invalid: " + name);
        }
        return errors;
    }

}
