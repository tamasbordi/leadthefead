/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.restcontroller;

import com.mycompany.leadthefeedproject.dto.AddUserByAdminDTO;
import com.mycompany.leadthefeedproject.dto.HttpErrorResponseDTO;
import com.mycompany.leadthefeedproject.dto.UserListDTO;
import com.mycompany.leadthefeedproject.dto.VerifyRegistrationByUserDTO;
import com.mycompany.leadthefeedproject.dto.VerifyRegistrationResponseByTokenDTO;
import com.mycompany.leadthefeedproject.entities.AuthorityRole;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.service.TokenService;
import com.mycompany.leadthefeedproject.service.UserEntityService;
import com.mycompany.leadthefeedproject.validations.EmailValidation;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author andras
 */
@RestController
public class UserRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

    private final EmailValidation emailValidation;

    private final UserEntityService ues;

    private final TokenService tokenService;

    private HttpErrorResponseDTO error = new HttpErrorResponseDTO();

    private String errorKey = "";

    @Autowired
    public UserRestController(EmailValidation emailValidation, UserEntityService ues, TokenService tokenService) {
        this.emailValidation = emailValidation;
        this.ues = ues;
        this.tokenService = tokenService;
    }

    @RequestMapping(path = "adduser", method = RequestMethod.GET)
    public ResponseEntity createUserByAdmin() {
        List<String> roles = ues.getRoles();
        if (roles.isEmpty()) {
            errorKey = "";
            error.getErrors().clear();
            errorKey = "database-failure";
            error.addError(errorKey);
            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity(roles, HttpStatus.OK);
    }

    @RequestMapping(path = "adduser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createUserByAdmin(@Valid @RequestBody AddUserByAdminDTO addUser, BindingResult result) {

        UserEntity ret = new UserEntity();

        if (!result.hasErrors()) {
            boolean isValidEmail = emailValidation.validateEmail(addUser.getEmail());
            if (isValidEmail) {
                boolean userExists = ues.isUserExistByEmailAddress(addUser.getEmail());
                if (!userExists) {
                    ret = ues.createUserWithoutPassword(addUser.getUsername(), addUser.getEmail(), addUser.getRoles());
                    saveRolesOfUser(ret);
                } else {
                    errorKey = "";
                    error.getErrors().clear();
                    errorKey = "already-existing-email-address";
                    error.addError(errorKey);
                    return new ResponseEntity(error, HttpStatus.FORBIDDEN);
                }
            } else {
                errorKey = "";
                error.getErrors().clear();
                errorKey = "invalid-email-address";
                error.addError(errorKey);
                return new ResponseEntity(error, HttpStatus.FORBIDDEN);
            }
        } else {
            errorKey = "";
            error.getErrors().clear();
            errorKey = "wrong-request";
            error.addError(errorKey);
            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity(new VerifyRegistrationResponseByTokenDTO(ret.getUsername(), ret.getEmailAddress()), HttpStatus.OK);
    }

    @RequestMapping(path = "verify-registration/{token}", method = RequestMethod.GET)
    public ResponseEntity verifiRegistrationByToken(@PathVariable String token) {
        boolean isValidToken = tokenService.isValidToken(token);
        if (isValidToken) {
            return new ResponseEntity(tokenService.getUserByToken(token), HttpStatus.OK);
        } else {
            errorKey = "";
            error.getErrors().clear();
            errorKey = "invalid-token";
            error.addError(errorKey);
            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(path = "verify-registration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity verifiRegistrationByUser(@Valid @RequestBody VerifyRegistrationByUserDTO verifyUser, BindingResult result) {
        if (!result.hasErrors()) {
            if (verifyUser.getPassword().equals(verifyUser.getConfirmPassword())) {
                ues.modifyUser(verifyUser);
                return new ResponseEntity(HttpStatus.OK);
            } else {
                errorKey = "";
                error.getErrors().clear();
                errorKey = "the-two-passwords-not-equal";
                error.addError(errorKey);
                return new ResponseEntity(error, HttpStatus.FORBIDDEN);
            }
        } else {
            errorKey = "";
            error.getErrors().clear();
            errorKey = "invalid-data";
            error.addError(errorKey);
            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(path = "/userlist", method = RequestMethod.GET)
    public ResponseEntity listUser() {
        List<UserListDTO> ret = ues.listUser();
        if (ret.isEmpty()) {
            errorKey = "";
            error.getErrors().clear();
            errorKey = "database-failure";
            error.addError(errorKey);
            return new ResponseEntity(error, HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity(ret, HttpStatus.OK);
    }

    @RequestMapping(path = "/csrf", method = RequestMethod.GET)
    public CsrfToken getCsrfToken(CsrfToken csrf) {
        return csrf;
    }

    private void saveRolesOfUser(UserEntity ret) {
        List<AuthorityRole> roles = ret.getRoles();
        for (int i = 0; i < roles.size(); i++) {
            String roleName = roles.get(i).getRoleName();
            ues.setRolesOfUser(ret, roleName);
        }
    }
    
    @RequestMapping(path = "/checkroles", method = RequestMethod.GET)
    public List<String> giveRolesOfUser() {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<AuthorityRole> authorities = user.getRoles();
        List<String> roles = new ArrayList<>();
        for (AuthorityRole a : authorities) {
            roles.add(a.getRoleName());
        }
        return roles;
    }

}
