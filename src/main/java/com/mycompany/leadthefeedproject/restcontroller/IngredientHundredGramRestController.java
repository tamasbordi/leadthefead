/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.restcontroller;

import com.mycompany.leadthefeedproject.dto.IngredientHundredGramDTO;
import com.mycompany.leadthefeedproject.entities.IngredientHundredGram;
import com.mycompany.leadthefeedproject.enums.IngredientType;
import com.mycompany.leadthefeedproject.service.IngredientHundredGramService;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@RestController
public class IngredientHundredGramRestController {
    
    private IngredientHundredGramService dbConnection;
    
    @Autowired
    public IngredientHundredGramRestController(IngredientHundredGramService dbConnection) {
        this.dbConnection = dbConnection;
    }
    
    @RequestMapping (path = "ingredients", method = RequestMethod.GET)
    public List<IngredientHundredGramDTO> giveAllIngredtiens() {
        List<IngredientHundredGramDTO> returnJson = new ArrayList<>();
        List<IngredientHundredGram> allIngredients = dbConnection.getAllIngredients();
        for (IngredientHundredGram i : allIngredients) {
            Boolean allergenCelery = i.getAllergenCelery();
            Boolean allergenEgg = i.getAllergenEgg();
            Boolean allergenFish = i.getAllergenFish();
            Boolean allergenGluten = i.getAllergenGluten();
            Boolean allergenLupin = i.getAllergenLupin();
            Boolean allergenMilk = i.getAllergenMilk();
            Boolean allergenMolluscs = i.getAllergenMolluscs();
            Boolean allergenMustard = i.getAllergenMustard();
            Boolean allergenPeanut = i.getAllergenPeanut();
            Boolean allergenSesame = i.getAllergenSesame();
            Boolean allergenShellfish = i.getAllergenShellfish();
            Boolean allergenSoy = i.getAllergenSoy();
            Boolean allergenSulpithes = i.getAllergenSulpithes();
            Boolean allergenWalnut = i.getAllergenWalnut();
            double calcium = i.getCalcium();
            double carbohydrate = i.getCarbohydrate();
            double energyKJ = i.getEnergyKJ();
            double energyKcal = i.getEnergyKcal();
            double fat = i.getFat();
            double fibre = i.getFibre();
            int glycemicIndex = i.getGlycemicIndex();
            long ingredientId = i.getIngredientId();
            String ingredientName = i.getIngredientName();
            IngredientType ingredientType = i.getIngredientType();
            LocalDateTime lastModified = i.getLastModified();
            String username = i.getLastModifier().getUsername();
            double magnesium = i.getMagnesium();
            double natrium = i.getNatrium();
            double potassium = i.getPotassium();
            double protein = i.getProtein();
            Boolean rawIngredient = i.getRawIngredient();
            double saturatedFat = i.getSaturatedFat();
            double sugar = i.getSugar();
            IngredientHundredGramDTO dto = new IngredientHundredGramDTO(ingredientId, rawIngredient, ingredientType, ingredientName, energyKJ, energyKcal, protein, fat, saturatedFat, carbohydrate, sugar, glycemicIndex, fibre, natrium, potassium, calcium, magnesium, allergenMilk, allergenGluten, allergenEgg, allergenShellfish, allergenFish, allergenMolluscs, allergenPeanut, allergenWalnut, allergenSesame, allergenSoy, allergenCelery, allergenMustard, allergenSulpithes, allergenLupin, lastModified, username);
            returnJson.add(dto);
        }
        return returnJson;
    }
    
    
}
