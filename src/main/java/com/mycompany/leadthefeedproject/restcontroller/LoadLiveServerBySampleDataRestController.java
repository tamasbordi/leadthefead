/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.restcontroller;

import com.mycompany.leadthefeedproject.dto.HttpErrorResponseDTO;
import com.mycompany.leadthefeedproject.entities.Allergen;
import com.mycompany.leadthefeedproject.entities.IngredientHundredGram;
import com.mycompany.leadthefeedproject.entities.IngredientQuantityInRecepie;
import com.mycompany.leadthefeedproject.entities.MainGroup;
import com.mycompany.leadthefeedproject.entities.Meal;
import com.mycompany.leadthefeedproject.entities.Menu;
import com.mycompany.leadthefeedproject.entities.Recepie;
import com.mycompany.leadthefeedproject.entities.SubGroup;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.enums.AgeGroup;
import com.mycompany.leadthefeedproject.enums.MealTime;
import com.mycompany.leadthefeedproject.enums.WeekDay;
import com.mycompany.leadthefeedproject.service.AllergenService;
import com.mycompany.leadthefeedproject.service.AuthorityRoleService;
import com.mycompany.leadthefeedproject.service.IngredientHundredGramService;
import com.mycompany.leadthefeedproject.service.IngredientQuantityInRecepieService;
import com.mycompany.leadthefeedproject.service.MainGroupService;
import com.mycompany.leadthefeedproject.service.MealService;
import com.mycompany.leadthefeedproject.service.MenuService;
import com.mycompany.leadthefeedproject.service.RecepieService;
import com.mycompany.leadthefeedproject.service.SubGroupRecepieHistoryService;
import com.mycompany.leadthefeedproject.service.SubGroupService;
import com.mycompany.leadthefeedproject.service.UserEntityService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@RestController
public class LoadLiveServerBySampleDataRestController {

    @Autowired
    ApplicationContext context;

    @RequestMapping(path = "loaddbwithsampedatas", method = RequestMethod.POST)
    public ResponseEntity loadAppWithSampleDatas() {
        HttpErrorResponseDTO errorList = new HttpErrorResponseDTO();
        List<String> errors = errorList.getErrors();
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (! user.isAdmin()) {
            errors.add("for this method admin role required");
            return new ResponseEntity(errorList, HttpStatus.FORBIDDEN);
        }
        AuthorityRoleService roleService = context.getBean(AuthorityRoleService.class);
        UserEntityService userService = context.getBean(UserEntityService.class);
        AllergenService allergenService = context.getBean(AllergenService.class);
        IngredientHundredGramService ingredientService = context.getBean(IngredientHundredGramService.class);
        IngredientQuantityInRecepieService ingredientQuantityService = context.getBean(IngredientQuantityInRecepieService.class);
        RecepieService recepieService = context.getBean(RecepieService.class);
        MealService mealService = context.getBean(MealService.class);
        MenuService menuService = context.getBean(MenuService.class);
        MainGroupService groupService = context.getBean(MainGroupService.class);
        SubGroupService subGroupService = context.getBean(SubGroupService.class);
        SubGroupRecepieHistoryService historyService = context.getBean(SubGroupRecepieHistoryService.class);

        createSampleUsers(userService, roleService);

        createSampleRecepies(ingredientService, ingredientQuantityService, userService, recepieService);
        createSampleMeals(userService, mealService, recepieService);
        createSampleMenus(menuService, userService, mealService);
        createSampleGroups(userService, groupService, subGroupService, allergenService);
        createSampleSubGroupHistory(historyService, subGroupService, menuService, userService);
        return new ResponseEntity(HttpStatus.OK);
    }

    private void createSampleUsers(UserEntityService userService, AuthorityRoleService roleService) {
        if (!userService.isUserExists("Példa Paula")) {
            userService.createUserWithPassword("Példa Paula", "peldapaula@leedthefeed.hu", "1234");
            UserEntity user = (UserEntity) userService.loadUserByUsername("peldapaula@leedthefeed.hu");
            userService.setRolesOfUser(user, "NUTRITIONIST");
            userService.setRolesOfUser(user, "FEEDING_MANAGER");
        }
        if (!userService.isUserExists("Teszt Elek")) {
            userService.createUserWithPassword("Teszt Elek", "tesztelek@leedthefeed.hu", "1234");
            UserEntity user = (UserEntity) userService.loadUserByUsername("tesztelek@leedthefeed.hu");
            userService.setRolesOfUser(user, "FEEDING_MANAGER");
        }
    }

    private void createSampleRecepies(IngredientHundredGramService ingredientService, IngredientQuantityInRecepieService ingredientQuantityService,
            UserEntityService userService, RecepieService recepieService) {
        for (int i = 0; i < 30; i++) {
            UserEntity admin = (UserEntity) userService.loadUserByUsername("admin@leedthefeed.hu");
            Recepie recepie = new Recepie(true, "Random Recept " + i, "Reference person " + i, LocalDateTime.now(), admin);
            recepieService.addNewRecepie(recepie);
            for (int j = 0; j < 4; j++) {
                List<IngredientHundredGram> allIngredients = ingredientService.getAllIngredients();
                int id = (int) (Math.random() * allIngredients.size());
                IngredientHundredGram ingredient = allIngredients.get(id);
                double quantity = (double) Math.random() * 200 + 1;
                IngredientQuantityInRecepie ingredientQuantity = new IngredientQuantityInRecepie(recepie, ingredient, quantity, ingredient.getEnergyKcal() * (quantity / 100));
                ingredientQuantityService.addIngredientQuantityInRecepie(ingredientQuantity);
            }
        }
    }

    private void createSampleMeals(UserEntityService userService, MealService mealService, RecepieService recepieService) {
        UserEntity admin = (UserEntity) userService.loadUserByUsername("admin@leedthefeed.hu");
        for (int i = 0; i < 10; i++) {
            mealService.addNewMeal(new Meal(WeekDay.MONDAY, MealTime.BREAKFAST, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.MONDAY, MealTime.FORENOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.MONDAY, MealTime.LUNCH, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.MONDAY, MealTime.AFTERNOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.MONDAY, MealTime.DINNER, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.TUESDAY, MealTime.BREAKFAST, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.TUESDAY, MealTime.FORENOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.TUESDAY, MealTime.LUNCH, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.TUESDAY, MealTime.AFTERNOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.TUESDAY, MealTime.DINNER, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.WEDNESDAY, MealTime.BREAKFAST, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.WEDNESDAY, MealTime.FORENOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.WEDNESDAY, MealTime.LUNCH, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.WEDNESDAY, MealTime.AFTERNOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.WEDNESDAY, MealTime.DINNER, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.THURSDAY, MealTime.BREAKFAST, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.THURSDAY, MealTime.FORENOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.THURSDAY, MealTime.LUNCH, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.THURSDAY, MealTime.AFTERNOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.THURSDAY, MealTime.DINNER, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.FRIDAY, MealTime.BREAKFAST, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.FRIDAY, MealTime.FORENOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.FRIDAY, MealTime.LUNCH, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.FRIDAY, MealTime.AFTERNOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.FRIDAY, MealTime.DINNER, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SATURDAY, MealTime.BREAKFAST, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SATURDAY, MealTime.FORENOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SATURDAY, MealTime.LUNCH, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SATURDAY, MealTime.AFTERNOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SATURDAY, MealTime.DINNER, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SUNDAY, MealTime.BREAKFAST, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SUNDAY, MealTime.FORENOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SUNDAY, MealTime.LUNCH, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SUNDAY, MealTime.AFTERNOON_SNACK, LocalDateTime.now(), admin));
            mealService.addNewMeal(new Meal(WeekDay.SUNDAY, MealTime.DINNER, LocalDateTime.now(), admin));
        }
        List<Meal> allMeals = mealService.getAllTheMealsWithRecepies();
        for (Meal meal : allMeals) {
            for (int i = 0; i <= 1; i++) {
                List<Recepie> allRecepie = recepieService.getAllRecepieWithIngredients();
                int recepieId = (int) (Math.random() * allRecepie.size());
                Recepie recepie = allRecepie.get(recepieId);
                mealService.addRecepieToMeal(meal, recepie);
            }
        }

    }

    private void createSampleMenus(MenuService menuService, UserEntityService userService, MealService mealService) {
        UserEntity admin = (UserEntity) userService.loadUserByUsername("admin@leedthefeed.hu");
        for (int i = 0; i < 20; i++) {
            Menu menu = new Menu("Random" + i, LocalDateTime.now(), admin);
            menuService.addNewMenu(menu);
        }
        List<Menu> allMenu = menuService.getAllTheMenus();
        WeekDay[] weekdays = WeekDay.values();
        for (Menu menu : allMenu) {
            for (int i = 0; i < weekdays.length; i++) {
                WeekDay day = weekdays[i];
                List<Meal> breakfastMeals = mealService.getAllMealsForMealTime(MealTime.BREAKFAST, day);
                List<Meal> foreNoonSnacktMeals = mealService.getAllMealsForMealTime(MealTime.FORENOON_SNACK, day);
                List<Meal> lunchMeals = mealService.getAllMealsForMealTime(MealTime.LUNCH, day);
                List<Meal> afternoonSnackMeals = mealService.getAllMealsForMealTime(MealTime.AFTERNOON_SNACK, day);
                List<Meal> dinnerMeals = mealService.getAllMealsForMealTime(MealTime.DINNER, day);
                int index = (int) (Math.random() * breakfastMeals.size());
                mealService.addMealToMenu(breakfastMeals.get(index), menu);
                mealService.addMealToMenu(foreNoonSnacktMeals.get(index), menu);
                mealService.addMealToMenu(lunchMeals.get(index), menu);
                mealService.addMealToMenu(afternoonSnackMeals.get(index), menu);
                mealService.addMealToMenu(dinnerMeals.get(index), menu);
            }
        }
    }

    private void createSampleGroups(UserEntityService userService, MainGroupService groupService, SubGroupService subGroupService, AllergenService allergenService) {
        UserEntity admin = (UserEntity) userService.loadUserByUsername("admin@leedthefeed.hu");
        for (int i = 0; i < 5; i++) {
            MainGroup newGroup = new MainGroup("Random Csoport " + i, LocalDateTime.now(), admin);
            groupService.addNewMainGroup(newGroup);
        }
        List<MainGroup> allGroup = groupService.getAllTheGroup();
        int counter = 0;
        for (MainGroup group : allGroup) {
            for (int i = 0; i < 3; i++) {
                SubGroup subGroup = new SubGroup(group, "Random Alcsoport " + i + counter, AgeGroup.ONE_TO_THREE, (int) (Math.random() * 10 + 1), LocalDateTime.now(), admin);
                subGroupService.addNewSubGroup(subGroup);
            }
            counter++;
        }
        List<SubGroup> allSubGroup = subGroupService.getAllTheSubGroup();
        List<Allergen> allAllergen = allergenService.getAllAllergen();
        for (SubGroup subGroup : allSubGroup) {
            int numOfAllergen = (int) (Math.random() * 4);
            List<Allergen> listOfSubGroup = new ArrayList<>();
            for (int i = 0; i < numOfAllergen; i++) {
                int index = (int) (Math.random() * allAllergen.size());
                Allergen a = allAllergen.get(index);
                if (!listOfSubGroup.contains(a)) {
                    listOfSubGroup.add(a);
                }
            }
            subGroupService.addAllergensToSubGroup(subGroup, listOfSubGroup);
            subGroupService.adjustMaxDailyNatrium(subGroup, Math.random() * 2);
            subGroupService.adjustMaxEnergyKcal(subGroup, 1300);
        }
    }

    private void createSampleSubGroupHistory(SubGroupRecepieHistoryService historyService, SubGroupService subGroupService, MenuService menuService, UserEntityService userService) {
        UserEntity admin = (UserEntity) userService.loadUserByUsername("admin@leedthefeed.hu");
        List<SubGroup> allTheSubGroup = subGroupService.getAllTheSubGroup();
        Random r = new Random();
        for (SubGroup subGroup : allTheSubGroup) {
            List<Menu> allTheMenus = menuService.getAllTheMenusWithMeals();
            Menu menu = allTheMenus.get(r.nextInt(allTheMenus.size()));
            historyService.saveValidatedMenuIntoSubGroupHistory(subGroup, menu, admin, LocalDate.of(2019, 02, 18));
        }
    }

}
