/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.Allergen;
import com.mycompany.leadthefeedproject.entities.MainGroup;
import com.mycompany.leadthefeedproject.entities.SubGroup;
import com.mycompany.leadthefeedproject.enums.AgeGroup;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class SubGroupService {
    
    @PersistenceContext
    EntityManager em;
    
    @Transactional
    public void addNewSubGroup(SubGroup subgroup) {
        em.persist(subgroup);
    }
    
    @Transactional
    public void removeSubGroup(SubGroup subgroup) {
        em.remove(subgroup);
    }
    
    @Transactional
    public void addAllergensToSubGroup(SubGroup subgroup, List<Allergen> allergens) {
        SubGroup find = em.find(SubGroup.class, subgroup.getSubGroupId());
        List<Allergen> allergenList = find.getAllergens();
        allergenList.addAll(allergens);
        find.setAllergens(allergenList);
    }
    
    @Transactional
    public void addSubGroupToMainGroup(SubGroup subgroup, MainGroup maingroup) {
        SubGroup find = em.find(SubGroup.class, subgroup.getSubGroupId());
        find.setGroup(maingroup);
    }
    
    @Transactional
    public List<SubGroup> getAllTheSubGroup() {
        return em.createQuery("SELECT g FROM SubGroup g").getResultList();
    }
    
    @Transactional
    public List<SubGroup> getSubGroupsOfUserWithMainGroupsAndAllergens(long userId) {
        EntityGraph<?> eg = em.getEntityGraph("subGroupWithAllergens");
        return em.createQuery("SELECT g FROM SubGroup g WHERE userOwned.id = :userId")
                .setParameter("userId", userId)
                .setHint("javax.persistence.loadgraph", eg).getResultList();
    }
    
    @Transactional
    public SubGroup findSubGroupOfUserByName (String name, long userid) throws NoResultException {
        return (SubGroup) em.createQuery("SELECT g FROM SubGroup g WHERE userOwned.id = :userid AND subGroupName = :name")
                .setParameter("name", name)
                .setParameter("userid", userid)
                .getSingleResult();
    }
    
    @Transactional
    public void updateSubGroup(Long subGroupId, String subGroupName, AgeGroup ageGroup, int numberOfPersons, double maxDailyEnergyKJ
            , double maxDailyEnergyKcal, double maxDailyProtein, double maxDailyFat, double maxDailySaturatedFat
            , double maxDailyCarbohydrate, double maxDailySugar, int maxGlycemicIndexPerMeal, double maxDailyFibre
            , double maxDailyNatrium, double maxDailyPotassium, double maxDailyCalcium, double maxDailyMagnesium
            , LocalDateTime lastModified, List<Allergen> allergens) {
        SubGroup find = em.find(SubGroup.class, subGroupId);
        find.setSubGroupName(subGroupName);
        find.setAgeGroup(ageGroup);
        find.setNumberOfPersons(numberOfPersons);
        find.setMaxDailyEnergyKJ(maxDailyEnergyKJ);
        find.setMaxDailyEnergyKcal(maxDailyEnergyKcal);
        find.setMaxDailyProtein(maxDailyProtein);
        find.setMaxDailyFat(maxDailyFat);
        find.setMaxDailySaturatedFat(maxDailySaturatedFat);
        find.setMaxDailyCarbohydrate(maxDailyCarbohydrate);
        find.setMaxDailySugar(maxDailySugar);
        find.setMaxGlycemicIndexPerMeal(maxGlycemicIndexPerMeal);
        find.setMaxDailyFibre(maxDailyFibre);
        find.setMaxDailyNatrium(maxDailyNatrium);
        find.setMaxDailyPotassium(maxDailyPotassium);
        find.setMaxDailyCalcium(maxDailyCalcium);
        find.setMaxDailyMagnesium(maxDailyMagnesium);
        find.setLastModified(lastModified);
        find.setAllergens(allergens);
    }
    
    @Transactional
    public void adjustMaxEnergyKcal(SubGroup subgroup, double max) {
        SubGroup find = em.find(SubGroup.class, subgroup.getSubGroupId());
        find.setMaxDailyEnergyKcal(max);
    }
    
    @Transactional
    public void adjustMaxDailyNatrium(SubGroup subgroup, double max) {
        SubGroup find = em.find(SubGroup.class, subgroup.getSubGroupId());
        find.setMaxDailyNatrium(max);
    }
}
