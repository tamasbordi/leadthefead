/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.IngredientQuantityInRecepie;
import com.mycompany.leadthefeedproject.entities.Meal;
import com.mycompany.leadthefeedproject.entities.Menu;
import com.mycompany.leadthefeedproject.entities.Recepie;
import com.mycompany.leadthefeedproject.enums.MealTime;
import com.mycompany.leadthefeedproject.enums.WeekDay;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class MealService {
    
    @Autowired
    ApplicationContext context;
    
    @PersistenceContext
    EntityManager em;

    @Transactional
    public void addNewMeal(Meal newMeal) {
        em.persist(newMeal);
    }
    
     @Transactional    
    public Meal findMealWithRecepies(long id) {
        EntityGraph eg = em.getEntityGraph("mealWithRecepies");
        Meal meal = (Meal) em.createQuery("Select meal FROM Meal meal WHERE mealId = :id")
                .setParameter("id", id)
                .setHint("javax.persistence.loadgraph", eg)
                .getSingleResult();
        return meal;
    }
    
    @Transactional
    public void addRecepieToMeal (Meal meal, Recepie recepie) {
        Meal find = em.find(Meal.class, meal.getMealId());
        List<Recepie> recepies = find.getRecepies();
        recepies.add(recepie);
        find.setRecepies(recepies);
    }
    
    @Transactional
    public void addMealToMenu (Meal meal, Menu menu) {
        Meal find = em.find(Meal.class, meal.getMealId());
        List<Menu> menusIncluding = find.getMenusIncluding();
        menusIncluding.add(menu);
        find.setMenusIncluding(menusIncluding);
    }
    
    @Transactional
    public void deleteMealFromMenu (Meal meal, Menu menu) {
        Meal findMeal = em.find(Meal.class, meal.getMealId());
        Menu findMenu = em.find(Menu.class, menu.getMenuId());
        List<Menu> menusIncluding = findMeal.getMenusIncluding();
        menusIncluding.remove(findMenu);
        findMeal.setMenusIncluding(menusIncluding);
    }
    
    @Transactional
    public List<Meal> getAllMealsForMealTime(MealTime mealTime, WeekDay day) {
        return em.createQuery("SELECT meal FROM Meal meal WHERE mealTime = :time AND day = :weekday")
                .setParameter("time", mealTime)
                .setParameter("weekday", day)
                .getResultList();
    }

    @Transactional
    public List<Meal> getAllTheMealsWithRecepies() {
        EntityGraph eg = em.getEntityGraph("mealWithRecepies");
        return em.createQuery("SELECT meal FROM Meal meal").setHint("javax.persistence.loadgraph", eg).getResultList();
    }
    
    @Transactional
    public Meal getMealWithRecepies(long mealId) {
        EntityGraph eg = em.getEntityGraph("mealWithRecepies");
        return (Meal) em.createQuery("SELECT meal FROM Meal meal WHERE mealId = :mealId")
                .setParameter("mealId", mealId)
                .setHint("javax.persistence.loadgraph", eg)
                .getSingleResult();
    }
    
    @Transactional
    public List<Meal> getMealsWithRecepiesOfUser(long userId) {
        EntityGraph eg = em.getEntityGraph("mealWithRecepies");
        return em.createQuery("SELECT meal FROM Meal meal WHERE userOwned.id = :userId")
                .setParameter("userId", userId)
                .setHint("javax.persistence.loadgraph", eg)
                .getResultList();
    }
    
}
