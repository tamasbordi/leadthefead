/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.Allergen;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class AllergenService {

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void addAllergen(String allergenName) {
        Allergen find = em.find(Allergen.class, allergenName);
        if (find == null) {
            Allergen newAllergen = new Allergen(allergenName);
            em.persist(newAllergen);
        }

    }
    
    @Transactional
    public List<Allergen> getAllAllergen() {
        return em.createQuery("SELECT allergen FROM Allergen allergen").getResultList();
    }
}
