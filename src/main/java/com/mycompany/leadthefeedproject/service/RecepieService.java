/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.dto.RecepieIngredientDTO;
import com.mycompany.leadthefeedproject.entities.IngredientHundredGram;
import com.mycompany.leadthefeedproject.entities.IngredientQuantityInRecepie;
import com.mycompany.leadthefeedproject.entities.Meal;
import com.mycompany.leadthefeedproject.entities.Recepie;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class RecepieService {
    
    @Autowired
    ApplicationContext context;
    
    @PersistenceContext
    EntityManager em;
    
    @Transactional
    public void addNewRecepie(Recepie recepie) {
        em.persist(recepie);
    }
    
    @Transactional
    public Recepie findRecepieByID(long id) throws NoResultException {
        return em.find(Recepie.class, id);
    }
    
    @Transactional    
    public Recepie findRecepieWithMealsIncluding(long id) {
        EntityGraph eg = em.getEntityGraph("recepieWithMealsIncluding");
        Recepie recepie = (Recepie) em.createQuery("Select recepie FROM Recepie recepie WHERE recepieID = :id")
                .setParameter("id", id)
                .setHint("javax.persistence.loadgraph", eg)
                .getSingleResult();
        return recepie;
    }
    
    @Transactional    
    public Recepie findRecepieWithIngredients(long id) {
        EntityGraph eg = em.getEntityGraph("recepieWithIngredients");
        Recepie recepie = (Recepie) em.createQuery("Select recepie FROM Recepie recepie WHERE recepieID = :id")
                .setParameter("id", id)
                .setHint("javax.persistence.loadgraph", eg)
                .getSingleResult();
        return recepie;
    }
    
    @Transactional    
    public List<Recepie> getAllRecepieWithIngredients() {
        EntityGraph eg = em.getEntityGraph("recepieWithIngredients");
        return em.createQuery("Select recepie FROM Recepie recepie")
                .setHint("javax.persistence.loadgraph", eg)
                .getResultList();
    }
    
    @Transactional
    public Boolean isRecepieTableEmpty() {
        long id = 1;
        Recepie firstRow = em.find(Recepie.class, id);
        return firstRow == null;
    }
    
    @Transactional
    public void updateRecepie(long recepieID, String recepieName, String referencePerson, List<RecepieIngredientDTO> ingredients) {
        IngredientQuantityInRecepieService quantityService = context.getBean(IngredientQuantityInRecepieService.class);
        Recepie recepie = findRecepieWithIngredients(recepieID);
        recepie.setRecepieName(recepieName);
        recepie.setReferencePerson(referencePerson);
        for (IngredientQuantityInRecepie ingredient : recepie.getIngredients()) {
            quantityService.deleteIngredientQuantityInRecepieById(ingredient.getId());
        }        
        for (RecepieIngredientDTO i : ingredients) {
            IngredientHundredGram iHundredGram = em.find(IngredientHundredGram.class, i.getIngredientId());
            double quantity = i.getIngredientQuantity();
            IngredientQuantityInRecepie updatedQuantity = new IngredientQuantityInRecepie(recepie, iHundredGram, quantity, quantity * (iHundredGram.getEnergyKcal() / 100));
            quantityService.addIngredientQuantityInRecepie(updatedQuantity);
        }
        recepie.setLastModified(LocalDateTime.now());
    }
}
