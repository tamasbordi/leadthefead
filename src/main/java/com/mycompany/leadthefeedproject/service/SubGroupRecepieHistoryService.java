/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.IngredientQuantityInRecepie;
import com.mycompany.leadthefeedproject.entities.Meal;
import com.mycompany.leadthefeedproject.entities.Menu;
import com.mycompany.leadthefeedproject.entities.Recepie;
import com.mycompany.leadthefeedproject.entities.SubGroup;
import com.mycompany.leadthefeedproject.entities.SubGroupRecepieHistory;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.enums.AssigmentStatus;
import com.mycompany.leadthefeedproject.enums.IngredientType;
import com.mycompany.leadthefeedproject.enums.MealTime;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import com.mycompany.leadthefeedproject.enums.MealTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class SubGroupRecepieHistoryService {

    @Autowired
    ApplicationContext context;
    @PersistenceContext
    EntityManager em;

    @Transactional
    public void saveValidatedMenuIntoSubGroupHistory(SubGroup subGroup, Menu menu, UserEntity user, LocalDate dateFrom) {
        List<Meal> meals = menu.getMeals();
        for (Meal meal : meals) {
            long dayPlus = 0;
            switch (meal.getDay()) {
                case MONDAY:
                    dayPlus = 0;
                    break;
                case TUESDAY:
                    dayPlus = 1;
                    break;
                case WEDNESDAY:
                    dayPlus = 2;
                    break;
                case THURSDAY:
                    dayPlus = 3;
                    break;
                case FRIDAY:
                    dayPlus = 4;
                    break;
                case SATURDAY:
                    dayPlus = 5;
                    break;
                case SUNDAY:
                    dayPlus = 6;
                    break;
            }
            List<Recepie> recepies = context.getBean(MealService.class).findMealWithRecepies(meal.getMealId()).getRecepies();
            for (Recepie recepie : recepies) {
                List<IngredientQuantityInRecepie> ingredients = context.getBean(RecepieService.class).findRecepieWithIngredients(recepie.getRecepieID()).getIngredients();
                for (IngredientQuantityInRecepie ingredient : ingredients) {
                    SubGroupRecepieHistory historyLog = new SubGroupRecepieHistory(subGroup, AssigmentStatus.VALIDATED, recepie.getRecepieName(),
                            meal.getMealTime(), dateFrom.plusDays(dayPlus), user, ingredient.getIngredient().getIngredientType(), ingredient.getIngredient().getIngredientName(),
                            ingredient.getIngredient().getEnergyKJ() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getEnergyKcal() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getProtein() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getFat() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getSaturatedFat() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getCarbohydrate() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getSugar() * (ingredient.getIngredientQuantityGram() / 100),
                            calculateGlycemicIndexOfRecepie(recepie),
                            ingredient.getIngredient().getFibre() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getNatrium() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getPotassium() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getCalcium() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getMagnesium() * (ingredient.getIngredientQuantityGram() / 100),
                            ingredient.getIngredient().getAllergenMilk(), ingredient.getIngredient().getAllergenGluten(),
                            ingredient.getIngredient().getAllergenEgg(), ingredient.getIngredient().getAllergenShellfish(),
                            ingredient.getIngredient().getAllergenFish(), ingredient.getIngredient().getAllergenMolluscs(),
                            ingredient.getIngredient().getAllergenPeanut(), ingredient.getIngredient().getAllergenWalnut(),
                            ingredient.getIngredient().getAllergenSesame(), ingredient.getIngredient().getAllergenSoy(),
                            ingredient.getIngredient().getAllergenCelery(), ingredient.getIngredient().getAllergenMustard(),
                            ingredient.getIngredient().getAllergenSulpithes(), ingredient.getIngredient().getAllergenLupin());
                    em.persist(historyLog);
                }
            }
        }
    }

    private int calculateGlycemicIndexOfRecepie(Recepie recepie) {
        //TODO
        return 0;
    }
}
