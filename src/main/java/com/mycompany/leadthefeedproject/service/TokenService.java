/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.dto.VerifyRegistrationResponseByTokenDTO;
import com.mycompany.leadthefeedproject.entities.Token;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.restcontroller.UserRestController;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author andras
 */
@Service
public class TokenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

    @PersistenceContext
    EntityManager em;

    @Transactional
    public Token makeTokenForNewUserAndSaveToDB(UserEntity user) {
        String emailAddress = user.getEmailAddress();
        Token token = new Token(emailAddress);
        em.persist(token);
        return token;
    }

    @Transactional
    public boolean isValidToken(String token) {
        List<String> tokenList = em.createQuery("SELECT token.token FROM Token token WHERE token.token = :token")
                .setParameter("token", token)
                .getResultList();
        return tokenList.size() == 1;
    }

    @Transactional
    public VerifyRegistrationResponseByTokenDTO getUserByToken(String token) {
        String emailAddress = (String) em.createQuery("SELECT token.emailAddress FROM Token token WHERE token.token = :token")
                .setParameter("token", token)
                .getSingleResult();
        UserEntity user = (UserEntity) em.createQuery("SELECT user FROM UserEntity user WHERE user.emailAddress = :emailAddress")
                .setParameter("emailAddress", emailAddress)
                .getSingleResult();
        return new VerifyRegistrationResponseByTokenDTO(user.getUsername(), user.getEmailAddress());
    }
}
