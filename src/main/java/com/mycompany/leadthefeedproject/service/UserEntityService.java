/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.dto.UserListDTO;
import com.mycompany.leadthefeedproject.dto.VerifyRegistrationByUserDTO;
import com.mycompany.leadthefeedproject.entities.AuthorityRole;
import com.mycompany.leadthefeedproject.entities.Token;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.restcontroller.UserRestController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author andras
 */
@Service
public class UserEntityService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private EmailServiceImpl emailService;

    @Transactional
    public UserEntity createUserWithoutPassword(String username, String emailAddress, String role) {

        List<String> roles = searchAllRolesByRoleName(role);

        List authList = em.createQuery("SELECT auth FROM AuthorityRole auth WHERE auth.roleName IN :roleNames")
                .setParameter("roleNames", roles)
                .getResultList();

        UserEntity user = new UserEntity(username, emailAddress, authList);

        em.persist(user);

        Token token = tokenService.makeTokenForNewUserAndSaveToDB(user);

        String url = "https://lead-the-feed.herokuapp.com/registration/" + token.getToken();
        emailService.sendSimpleMessage(token.getEmailAddress(), "Élelmezésvezetői rendszer regisztráció megerősítése", url);

        return user;
    }

    @Transactional
    public void createUserWithPassword(String username, String emailAddress, String password) {
        UserEntity user = new UserEntity(username, emailAddress, passwordEncoder.encode(password));
        em.persist(user);
    }

    @Transactional
    public void setRolesOfUser(UserEntity user, String role) {
        AuthorityRole newRole = em.find(AuthorityRole.class, role);
        List<UserEntity> roleUsers = newRole.getRoleUsers();
        roleUsers.add(user);
        newRole.setRoleUsers(roleUsers);
    }

    @Transactional
    public boolean isUserExists(String userName) {
        List resultList = em.createQuery("SELECT user FROM UserEntity user WHERE user.username = :userName")
                .setParameter("userName", userName)
                .getResultList();
        return !resultList.isEmpty();
    }

    @Transactional
    public boolean isUserExists(Long id) {
        UserEntity find = em.find(UserEntity.class, id);
        return find != null;
    }

    @Transactional
    public Boolean isUserExistByEmailAddress(String email) throws UsernameNotFoundException {
        List resultList = em.createQuery("SELECT user FROM UserEntity user WHERE user.emailAddress = :email")
                .setParameter("email", email)
                .getResultList();
        return 1 == resultList.size();
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserDetails user = (UserDetails) em.createQuery("SELECT user FROM UserEntity user WHERE user.emailAddress = :email")
                .setParameter("email", email)
                .getSingleResult();
        return user;
    }

    @Transactional
    public List<String> getRoles() {
        List<String> roles = em.createQuery("SELECT roles.roleName FROM AuthorityRole roles")
                .getResultList();
        return roles;
    }

    @Transactional
    public void modifyUser(VerifyRegistrationByUserDTO verifyUser) {
        UserEntity modifyUser = (UserEntity) loadUserByUsername(verifyUser.getEmailAddress());
        modifyUser.setPassword(passwordEncoder.encode(verifyUser.getConfirmPassword()));
        if (!modifyUser.getUsername().equals(verifyUser.getNewUsername())) {
            modifyUser.setUsername(verifyUser.getNewUsername());
        }
    }

    @Transactional
    public List<UserListDTO> listUser() {
        List<UserEntity> userList = em.createQuery("SELECT user FROM UserEntity user")
                .getResultList();
        List<UserListDTO> ret = new ArrayList();
        userList.forEach((userEntity) -> {

            String roleName = searchMostResponsibleRole(userEntity);
            ret.add(new UserListDTO(userEntity.getUsername(), userEntity.getEmailAddress(), roleName));

        });

        return ret;
    }

    private List<String> searchAllRolesByRoleName(String role) {
        List<String> ret = new ArrayList();
        List<AuthorityRole> resultList = em.createQuery("SELECT roles FROM AuthorityRole roles")
                .getResultList();
        int currentHierarchy = 0;
        for (AuthorityRole authorityRole : resultList) {
            if (role.equals(authorityRole.getRoleName())) {
                currentHierarchy = authorityRole.getHierarchy();
            }
        }
        for (AuthorityRole authorityRole : resultList) {
            if (currentHierarchy <= authorityRole.getHierarchy()) {
                ret.add(authorityRole.getRoleName());
            }
        }
        return ret;
    }

    private HashMap<String, Integer> createRolesWithPrio() {
        HashMap<String, Integer> ret = new HashMap();
        ret.put("ADMIN", 1);
        ret.put("NUTRITIONIST", 2);
        ret.put("FEEDING_MANAGER", 3);
        return ret;
    }

    private String searchMostResponsibleRole(UserEntity userEntity) {
        List<AuthorityRole> roles = userEntity.getRoles();
        String roleName = "";
        int maxHierarchy = 1000;
        for (AuthorityRole role : roles) {
            if (role.getHierarchy() < maxHierarchy) {
                roleName = role.getRoleName();
                maxHierarchy = role.getHierarchy();
            }
        }
        return roleName;
    }
}
