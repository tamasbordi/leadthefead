/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.AuthorityRole;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class AuthorityRoleService {

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void createNewRole(String roleName, int hierarchy) {
        AuthorityRole role = em.find(AuthorityRole.class, roleName);
        if (role == null) {
            AuthorityRole authorityRole = new AuthorityRole(roleName, hierarchy);
            em.persist(authorityRole);
        }
    }

    @Transactional
    public AuthorityRole getRoleByRoleName(String roleName) {
        return em.find(AuthorityRole.class, roleName);
    }
}
