/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.MainGroup;
import com.mycompany.leadthefeedproject.entities.SubGroup;
import java.util.List;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class MainGroupService {

    @Autowired
    ApplicationContext context;

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void addNewMainGroup(MainGroup group) {
        em.persist(group);
    }

    @Transactional
    public List<MainGroup> getAllTheGroup() {
        return em.createQuery("SELECT g FROM MainGroup g").getResultList();
    }
    
    @Transactional
    public MainGroup findMainGroupById(long id) throws NoResultException {
        return (MainGroup) em.createQuery("SELECT g FROM MainGroup g WHERE groupId = : id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Transactional
    public MainGroup findMainGroupOfUserByName(String name, Long userid) throws NoResultException {
        return (MainGroup) em.createQuery("SELECT g FROM MainGroup g WHERE groupName = :name AND userOwned.id = :userid")
                .setParameter("name", name)
                .setParameter("userid", userid)
                .getSingleResult();
    }

}
