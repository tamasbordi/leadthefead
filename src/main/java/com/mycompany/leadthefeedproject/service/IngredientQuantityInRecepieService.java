/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.IngredientQuantityInRecepie;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class IngredientQuantityInRecepieService {
    
    @PersistenceContext
    EntityManager em;
    
    @Transactional
    public void addIngredientQuantityInRecepie(IngredientQuantityInRecepie ingredientWithQuantity) {
        em.persist(ingredientWithQuantity);
    }
    
    @Transactional
    public IngredientQuantityInRecepie findIngredientQuantityInRecepieById(long id) {
        return em.find(IngredientQuantityInRecepie.class, id);
    }
    
    @Transactional
    public void deleteIngredientQuantityInRecepieById(long id) {
        IngredientQuantityInRecepie find = em.find(IngredientQuantityInRecepie.class, id);
        em.remove(find);
    }
}
