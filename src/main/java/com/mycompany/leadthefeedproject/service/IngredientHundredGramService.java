/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.IngredientHundredGram;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.enums.IngredientType;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class IngredientHundredGramService {

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void addNewIngredient(IngredientHundredGram newIngredient) {
        em.persist(newIngredient);
    }
    
    @Transactional
    public IngredientHundredGram findIngredientById(long id) {
        return em.find(IngredientHundredGram.class, id);
    }
    
    @Transactional
    public Boolean isIngredientHundredGramTableEmpty() {
        long id = 1;
        IngredientHundredGram firstRow = em.find(IngredientHundredGram.class, id);
        return firstRow == null;
    }
    
    @Transactional
    public List<IngredientHundredGram> getAllIngredients() {
        return em.createQuery("SELECT i FROM IngredientHundredGram i").getResultList();
    }
}
