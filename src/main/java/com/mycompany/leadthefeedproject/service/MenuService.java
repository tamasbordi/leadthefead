/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.IngredientHundredGram;
import com.mycompany.leadthefeedproject.entities.IngredientQuantityInRecepie;
import com.mycompany.leadthefeedproject.entities.Meal;
import com.mycompany.leadthefeedproject.entities.Menu;
import com.mycompany.leadthefeedproject.entities.Recepie;
import com.mycompany.leadthefeedproject.enums.WeekDay;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class MenuService {

    @Autowired
    ApplicationContext context;

    @PersistenceContext
    EntityManager em;

    @Transactional
    public void addNewMenu(Menu menu) {
        em.persist(menu);
    }

    @Transactional
    public Menu getMenuWithMeals(long menuId) {
        EntityGraph<?> eg = em.getEntityGraph("menuWithMeals");
        return (Menu) em.createQuery("SELECT menu FROM Menu menu WHERE menuId = :id")
                .setParameter("id", menuId)
                .setHint("javax.persistence.loadgraph", eg)
                .getSingleResult();
    }

    @Transactional
    public List<Menu> getAllTheMenus() {
        return em.createQuery("SELECT menu FROM Menu menu").getResultList();
    }

    @Transactional
    public List<Menu> getAllTheMenusOfUser(long userId) {
        EntityGraph<?> eg = em.getEntityGraph("menuWithMeals");
        return em.createQuery("SELECT menu FROM Menu menu WHERE userOwned.id = :id")
                .setParameter("id", userId)
                .setHint("javax.persistence.loadgraph", eg)
                .getResultList();
    }

    @Transactional
    public List<Menu> getAllTheMenusWithMeals() {
        EntityGraph<?> eg = em.getEntityGraph("menuWithMeals");
        return em.createQuery("SELECT menu FROM Menu menu").setHint("javax.persistence.loadgraph", eg).getResultList();
    }


    @Transactional
    public void updateMenuName(long menuId, String name) {
        Menu find = em.find(Menu.class, menuId);
        find.setMenuName(name);
    }
}
