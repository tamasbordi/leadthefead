/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.LawMinMaxTimeOfIngredient;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class LawMinMaxTimeOfIngredientService {
    
    @PersistenceContext
    EntityManager em;
    
    @Transactional
    public void addNewLawMinMaxTimeOfIngredient(LawMinMaxTimeOfIngredient newLaw) {
        LawMinMaxTimeOfIngredient find = em.find(LawMinMaxTimeOfIngredient.class, newLaw.getIngredientType());
        if (find == null) {
            em.persist(newLaw);
        }        
    }
    
}
