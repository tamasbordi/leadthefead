/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.service;

import com.mycompany.leadthefeedproject.entities.LawMinMaxPerDayPerAgeGroup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
@Service
public class LawMinMaxPerDayPerAgeGroupService {
    
    @PersistenceContext
    EntityManager em;
    
    @Transactional
    public void addNewLawMinMaxPerDayPerAgeGroup(LawMinMaxPerDayPerAgeGroup newLaw) {
        LawMinMaxPerDayPerAgeGroup find = em.find(LawMinMaxPerDayPerAgeGroup.class, newLaw.getAgeGroup());
        if (find == null) {
            em.persist(newLaw);
        }
    }
}
