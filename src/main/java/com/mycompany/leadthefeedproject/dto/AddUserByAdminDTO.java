/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

import com.mycompany.leadthefeedproject.restcontroller.UserRestController;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author andras
 */
public class AddUserByAdminDTO {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

    @NotNull
    @NotEmpty
    private String username;

    @NotNull
    @NotEmpty
    private String email;

    @NotNull
    @NotEmpty
    private String roles;

    public AddUserByAdminDTO() {
    }

    public AddUserByAdminDTO(AddUserByAdminDTO addUser) {
        LOGGER.debug("adduser name : {}",addUser.getUsername());
        LOGGER.debug("adduser email : {}",addUser.getEmail());
        LOGGER.debug("adduser roles : {}",addUser.getRoles().toString());
        this.username = addUser.getUsername();
        this.email = addUser.getEmail();
        this.roles = addUser.getRoles();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

//    public void addRoles(List<String> roles) {
//        if (!roles.isEmpty()) {
//            for (String role : roles) {
//                this.roles.add(role);
//            }
//        }
//    }
}
