/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author andras
 */
public class VerifyRegistrationByUserDTO {

    @NotEmpty
    @NotNull
    private String emailAddress;

    @NotNull
    private String originalUsername;

    private String newUsername;

    @NotEmpty
    @NotNull
    @Size(min = 8)
    private String password;

    @NotEmpty
    @NotNull
    @Size(min = 8)
    private String confirmPassword;

    public VerifyRegistrationByUserDTO() {
    }

    public VerifyRegistrationByUserDTO(String username, String emailAddress, String password, String confirmPassword) {
        this.originalUsername = username;
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getOriginalUsername() {
        return originalUsername;
    }

    public void setOriginalUsername(String originalUsername) {
        this.originalUsername = originalUsername;
    }

    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
}
