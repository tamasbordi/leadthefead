/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public class HttpErrorResponseDTO {

    private List<String> errors = new ArrayList<>();

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public void addError(String errors) {
        this.errors.add(errors);
    }
}
