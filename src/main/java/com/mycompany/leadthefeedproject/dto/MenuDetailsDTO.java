/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

import com.mycompany.leadthefeedproject.enums.WeekDay;
import java.time.LocalDateTime;
import java.util.HashMap;


/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public class MenuDetailsDTO {

    private long menuId;
    private String menuName;
    private HashMap<WeekDay, MenuDetailDayDTO> weekDays;
    private LocalDateTime lastModified;
    private String userOwned;

    public MenuDetailsDTO() {
    }
    
    public MenuDetailsDTO(long menuId, String menuName, HashMap<WeekDay, MenuDetailDayDTO> weekDays, LocalDateTime lastModified, String userOwned) {
        this.menuId = menuId;
        this.menuName = menuName;
        this.weekDays = weekDays;
        this.lastModified = lastModified;
        this.userOwned = userOwned;
    }

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public HashMap<WeekDay, MenuDetailDayDTO> getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(HashMap<WeekDay, MenuDetailDayDTO> weekDays) {
        this.weekDays = weekDays;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public String getUserOwned() {
        return userOwned;
    }

    public void setUserOwned(String userOwned) {
        this.userOwned = userOwned;
    }

    
}
