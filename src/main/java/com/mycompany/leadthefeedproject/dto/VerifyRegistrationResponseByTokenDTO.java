/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

import javax.validation.constraints.NotNull;

/**
 *
 * @author andras
 */
public class VerifyRegistrationResponseByTokenDTO {

    @NotNull
    private String userName;

    @NotNull
    private String emailAddress;

    public VerifyRegistrationResponseByTokenDTO() {
    }

    public VerifyRegistrationResponseByTokenDTO(String userName, String emailAddress) {
        this.userName = userName;
        this.emailAddress = emailAddress;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

}
