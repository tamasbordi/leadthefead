/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public class RecepieIngredientDTO {
    
    private long ingredientId;
    private double ingredientQuantity;

    public RecepieIngredientDTO() {
    }
    
    public RecepieIngredientDTO(long ingredientId, double ingredientQuantity) {
        this.ingredientId = ingredientId;
        this.ingredientQuantity = ingredientQuantity;
    }

    public long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public double getIngredientQuantity() {
        return ingredientQuantity;
    }

    public void setIngredientQuantity(double ingredientQuantity) {
        this.ingredientQuantity = ingredientQuantity;
    }
    
    
    
}
