/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

import java.time.LocalDateTime;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public class MenuListDTO {
    
    private long menuId;
    private String menuName;
    int numberOfDays;
    private LocalDateTime lastModified;
    private String userOwned;

    public MenuListDTO(long menuId, String menuName, int numberOfDays, LocalDateTime lastModified, String userOwned) {
        this.menuId = menuId;
        this.menuName = menuName;
        this.numberOfDays = numberOfDays;
        this.lastModified = lastModified;
        this.userOwned = userOwned;
    }

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public double getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public String getUserOwned() {
        return userOwned;
    }

    public void setUserOwned(String userOwned) {
        this.userOwned = userOwned;
    }
    
    
}
