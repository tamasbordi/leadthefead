/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public class NewRecepieDTO {
    
    private long recepieID;
    private String recepieName;
    private String referencePerson;
    private List<RecepieIngredientDTO> ingredients;
    private LocalDateTime lastModified;
    private String userOwned;

    public NewRecepieDTO() {
    }

    public NewRecepieDTO(long recepieID, String recepieName, String referencePerson, List<RecepieIngredientDTO> ingredients, LocalDateTime lastModified, String userOwned) {
        this.recepieID = recepieID;
        this.recepieName = recepieName;
        this.referencePerson = referencePerson;
        this.ingredients = ingredients;
        this.lastModified = lastModified;
        this.userOwned = userOwned;
    }

    public long getRecepieID() {
        return recepieID;
    }

    public void setRecepieID(long recepieID) {
        this.recepieID = recepieID;
    }

    public String getRecepieName() {
        return recepieName;
    }

    public void setRecepieName(String recepieName) {
        this.recepieName = recepieName;
    }

    public String getReferencePerson() {
        return referencePerson;
    }

    public void setReferencePerson(String referencePerson) {
        this.referencePerson = referencePerson;
    }

    public List<RecepieIngredientDTO> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<RecepieIngredientDTO> ingredients) {
        this.ingredients = ingredients;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public String getUserOwned() {
        return userOwned;
    }

    public void setUserOwned(String userOwned) {
        this.userOwned = userOwned;
    }

        
}
