/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

import com.mycompany.leadthefeedproject.enums.WeekDay;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public class MenuDetailDayDTO {
    
    private List<Long> breakfastRecipeIDs = new ArrayList<>();
    private List<Long> forenoonSnackRecipeIDs = new ArrayList<>();
    private List<Long> lunchRecipeIDs = new ArrayList<>();
    private List<Long> afternoonSnackRecipeIDs = new ArrayList<>();
    private List<Long> dinnerSnackRecipeIDs = new ArrayList<>();

    public MenuDetailDayDTO() {
    }
    
    public List<Long> getBreakfastRecipeIDs() {
        return breakfastRecipeIDs;
    }

    public void setBreakfastRecipeIDs(List<Long> breakfastRecipeIDs) {
        this.breakfastRecipeIDs = breakfastRecipeIDs;
    }

    public List<Long> getForenoonSnackRecipeIDs() {
        return forenoonSnackRecipeIDs;
    }

    public void setForenoonSnackRecipeIDs(List<Long> forenoonSnackRecipeIDs) {
        this.forenoonSnackRecipeIDs = forenoonSnackRecipeIDs;
    }

    public List<Long> getLunchRecipeIDs() {
        return lunchRecipeIDs;
    }

    public void setLunchRecipeIDs(List<Long> lunchRecipeIDs) {
        this.lunchRecipeIDs = lunchRecipeIDs;
    }

    public List<Long> getAfternoonSnackRecipeIDs() {
        return afternoonSnackRecipeIDs;
    }

    public void setAfternoonSnackRecipeIDs(List<Long> afternoonSnackRecipeIDs) {
        this.afternoonSnackRecipeIDs = afternoonSnackRecipeIDs;
    }

    public List<Long> getDinnerSnackRecipeIDs() {
        return dinnerSnackRecipeIDs;
    }

    public void setDinnerSnackRecipeIDs(List<Long> dinnerSnackRecipeIDs) {
        this.dinnerSnackRecipeIDs = dinnerSnackRecipeIDs;
    }
    
    
}
