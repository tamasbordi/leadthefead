/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

import com.mycompany.leadthefeedproject.entities.Allergen;
import com.mycompany.leadthefeedproject.enums.AgeGroup;
import java.util.List;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public class NewGroupDTO {
    
    private Long mainGroupId;
    private String groupName;
    private Long subGroupId;
    private String subGroupName;
    private AgeGroup ageGroup;
    private int numberOfPersons;
    private double maxDailyEnergyKJ;
    private double maxDailyEnergyKcal;
    private double maxDailyProtein;
    private double maxDailyFat;
    private double maxDailySaturatedFat;
    private double maxDailyCarbohydrate;
    private double maxDailySugar;
    private int maxGlycemicIndexPerMeal;
    private double maxDailyFibre;
    private double maxDailyNatrium;
    private double maxDailyPotassium;
    private double maxDailyCalcium;
    private double maxDailyMagnesium;
    private List<Allergen> allergens;

    public NewGroupDTO() {
    }

    public NewGroupDTO(Long mainGroupId, String groupName, Long subGroupId, String subGroupName, AgeGroup ageGroup, int numberOfPersons, double maxDailyEnergyKJ, double maxDailyEnergyKcal, double maxDailyProtein, double maxDailyFat, double maxDailySaturatedFat, double maxDailyCarbohydrate, double maxDailySugar, int maxGlycemicIndexPerMeal, double maxDailyFibre, double maxDailyNatrium, double maxDailyPotassium, double maxDailyCalcium, double maxDailyMagnesium, List<Allergen> allergens) {
        this.mainGroupId = mainGroupId;
        this.groupName = groupName;
        this.subGroupId = subGroupId;
        this.subGroupName = subGroupName;
        this.ageGroup = ageGroup;
        this.numberOfPersons = numberOfPersons;
        this.maxDailyEnergyKJ = maxDailyEnergyKJ;
        this.maxDailyEnergyKcal = maxDailyEnergyKcal;
        this.maxDailyProtein = maxDailyProtein;
        this.maxDailyFat = maxDailyFat;
        this.maxDailySaturatedFat = maxDailySaturatedFat;
        this.maxDailyCarbohydrate = maxDailyCarbohydrate;
        this.maxDailySugar = maxDailySugar;
        this.maxGlycemicIndexPerMeal = maxGlycemicIndexPerMeal;
        this.maxDailyFibre = maxDailyFibre;
        this.maxDailyNatrium = maxDailyNatrium;
        this.maxDailyPotassium = maxDailyPotassium;
        this.maxDailyCalcium = maxDailyCalcium;
        this.maxDailyMagnesium = maxDailyMagnesium;
        this.allergens = allergens;
    }

    public Long getMainGroupId() {
        return mainGroupId;
    }

    public void setMainGroupId(Long mainGroupId) {
        this.mainGroupId = mainGroupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    
    public String getSubGroupName() {
        return subGroupName;
    }

    public void setSubGroupName(String subGroupName) {
        this.subGroupName = subGroupName;
    }

    public Long getSubGroupId() {
        return subGroupId;
    }

    public void setSubGroupId(Long subGroupId) {
        this.subGroupId = subGroupId;
    }
    
    

    public AgeGroup getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(AgeGroup ageGroup) {
        this.ageGroup = ageGroup;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public double getMaxDailyEnergyKJ() {
        return maxDailyEnergyKJ;
    }

    public void setMaxDailyEnergyKJ(double maxDailyEnergyKJ) {
        this.maxDailyEnergyKJ = maxDailyEnergyKJ;
    }

    public double getMaxDailyEnergyKcal() {
        return maxDailyEnergyKcal;
    }

    public void setMaxDailyEnergyKcal(double maxDailyEnergyKcal) {
        this.maxDailyEnergyKcal = maxDailyEnergyKcal;
    }

    public double getMaxDailyProtein() {
        return maxDailyProtein;
    }

    public void setMaxDailyProtein(double maxDailyProtein) {
        this.maxDailyProtein = maxDailyProtein;
    }

    public double getMaxDailyFat() {
        return maxDailyFat;
    }

    public void setMaxDailyFat(double maxDailyFat) {
        this.maxDailyFat = maxDailyFat;
    }

    public double getMaxDailySaturatedFat() {
        return maxDailySaturatedFat;
    }

    public void setMaxDailySaturatedFat(double maxDailySaturatedFat) {
        this.maxDailySaturatedFat = maxDailySaturatedFat;
    }

    public double getMaxDailyCarbohydrate() {
        return maxDailyCarbohydrate;
    }

    public void setMaxDailyCarbohydrate(double maxDailyCarbohydrate) {
        this.maxDailyCarbohydrate = maxDailyCarbohydrate;
    }

    public double getMaxDailySugar() {
        return maxDailySugar;
    }

    public void setMaxDailySugar(double maxDailySugar) {
        this.maxDailySugar = maxDailySugar;
    }

    public int getMaxGlycemicIndexPerMeal() {
        return maxGlycemicIndexPerMeal;
    }

    public void setMaxGlycemicIndexPerMeal(int maxGlycemicIndexPerMeal) {
        this.maxGlycemicIndexPerMeal = maxGlycemicIndexPerMeal;
    }

    public double getMaxDailyFibre() {
        return maxDailyFibre;
    }

    public void setMaxDailyFibre(double maxDailyFibre) {
        this.maxDailyFibre = maxDailyFibre;
    }

    public double getMaxDailyNatrium() {
        return maxDailyNatrium;
    }

    public void setMaxDailyNatrium(double maxDailyNatrium) {
        this.maxDailyNatrium = maxDailyNatrium;
    }

    public double getMaxDailyPotassium() {
        return maxDailyPotassium;
    }

    public void setMaxDailyPotassium(double maxDailyPotassium) {
        this.maxDailyPotassium = maxDailyPotassium;
    }

    public double getMaxDailyCalcium() {
        return maxDailyCalcium;
    }

    public void setMaxDailyCalcium(double maxDailyCalcium) {
        this.maxDailyCalcium = maxDailyCalcium;
    }

    public double getMaxDailyMagnesium() {
        return maxDailyMagnesium;
    }

    public void setMaxDailyMagnesium(double maxDailyMagnesium) {
        this.maxDailyMagnesium = maxDailyMagnesium;
    }

    public List<Allergen> getAllergens() {
        return allergens;
    }

    public void setAllergens(List<Allergen> allergens) {
        this.allergens = allergens;
    }

    
}
