/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.dto;

/**
 *
 * @author andras
 */
public class UserListDTO {

    private String username;

    private String emailAddress;

    private String role;

    public UserListDTO() {
    }

    public UserListDTO(String username, String emailAddress, String role) {
        this.username = username;
        this.emailAddress = emailAddress;
        this.role = role;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getRoles() {
        return role;
    }

    public void setRoles(String roles) {
        this.role = roles;
    }

}
