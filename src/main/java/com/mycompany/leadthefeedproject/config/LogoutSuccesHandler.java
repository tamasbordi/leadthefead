/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.config;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 *
 * @author andras
 */
public class LogoutSuccesHandler implements LogoutSuccessHandler {

    public LogoutSuccesHandler() {
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest arg0, HttpServletResponse arg1, Authentication arg2) throws IOException, ServletException {
        arg1.setHeader("Acces-Control-Allow-Origin", "http://localhost:4200, localhost:4200, https://lead-the-feed.herokuapp.com");
        arg1.setHeader("Access-Control-Allow-Credentials", "true");
        arg1.setStatus(200);
    }
    
}
