/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.config;

import com.mycompany.leadthefeedproject.entities.IngredientHundredGram;
import com.mycompany.leadthefeedproject.entities.LawMinMaxPerDayPerAgeGroup;
import com.mycompany.leadthefeedproject.entities.LawMinMaxTimeOfIngredient;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import com.mycompany.leadthefeedproject.enums.AgeGroup;
import com.mycompany.leadthefeedproject.enums.IngredientType;
import com.mycompany.leadthefeedproject.restcontroller.LoadLiveServerBySampleDataRestController;
import com.mycompany.leadthefeedproject.service.AllergenService;
import com.mycompany.leadthefeedproject.service.AuthorityRoleService;
import com.mycompany.leadthefeedproject.service.IngredientHundredGramService;
import com.mycompany.leadthefeedproject.service.LawMinMaxPerDayPerAgeGroupService;
import com.mycompany.leadthefeedproject.service.LawMinMaxTimeOfIngredientService;
import com.mycompany.leadthefeedproject.service.UserEntityService;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 *
 * @author andras
 */
@Component
public class StartAppInitDatabase {

    @EventListener(ContextRefreshedEvent.class)
    public void onAppStartup(ContextRefreshedEvent ev) throws ServletException {
        StartAppInitDatabase me = ev.getApplicationContext().getBean(StartAppInitDatabase.class);
        AuthorityRoleService roleService = ev.getApplicationContext().getBean(AuthorityRoleService.class);
        UserEntityService userService = ev.getApplicationContext().getBean(UserEntityService.class);
        AllergenService allergenService = ev.getApplicationContext().getBean(AllergenService.class);
        IngredientHundredGramService ingredientService = ev.getApplicationContext().getBean(IngredientHundredGramService.class);
        LawMinMaxTimeOfIngredientService lawMinMaxService = ev.getApplicationContext().getBean(LawMinMaxTimeOfIngredientService.class);
        LawMinMaxPerDayPerAgeGroupService lawMinMaxAgeGroupService = ev.getApplicationContext().getBean(LawMinMaxPerDayPerAgeGroupService.class);
        createAuthotories(roleService);
        createAdminUser(userService, roleService);
        setUpAllergenTable(allergenService);
        setUpLawMinMaxTimeOfIngredientTable(lawMinMaxService);
        setUpLawMinMaxPerDayPerAgeGroupTable(lawMinMaxAgeGroupService);
        if (ingredientService.isIngredientHundredGramTableEmpty()) {
            loadIngredientHundreadGramFromFile("SampleIngredientHundredGram.txt", userService, ingredientService);
        }
    }

    private void createAuthotories(AuthorityRoleService roleService) {
        roleService.createNewRole("ADMIN", 1);
        roleService.createNewRole("NUTRITIONIST", 2);
        roleService.createNewRole("FEEDING_MANAGER", 3);
    }

    private void createAdminUser(UserEntityService userService, AuthorityRoleService roleService) {
        if (!userService.isUserExists("Admin")) {
            userService.createUserWithPassword("Admin", "admin@leedthefeed.hu", "admin");
            UserEntity admin = (UserEntity) userService.loadUserByUsername("admin@leedthefeed.hu");
            userService.setRolesOfUser(admin, "ADMIN");
            userService.setRolesOfUser(admin, "NUTRITIONIST");
            userService.setRolesOfUser(admin, "FEEDING_MANAGER");
        }
    }

    private void setUpAllergenTable(AllergenService allergenService) {
        allergenService.addAllergen("MILK");
        allergenService.addAllergen("GLUTEN");
        allergenService.addAllergen("EGG");
        allergenService.addAllergen("SHELLFISH");
        allergenService.addAllergen("FISH");
        allergenService.addAllergen("MOLLUSCS");
        allergenService.addAllergen("PEANUT");
        allergenService.addAllergen("WALNUT");
        allergenService.addAllergen("SESAME");
        allergenService.addAllergen("SOY");
        allergenService.addAllergen("CELERY");
        allergenService.addAllergen("MUSTARD");
        allergenService.addAllergen("SULPITHES");
        allergenService.addAllergen("LUPIN");
    }

    private void setUpLawMinMaxTimeOfIngredientTable(LawMinMaxTimeOfIngredientService lawMinMaxService) {
        lawMinMaxService.addNewLawMinMaxTimeOfIngredient(new LawMinMaxTimeOfIngredient(IngredientType.CEREALS, 0, 4));
        lawMinMaxService.addNewLawMinMaxTimeOfIngredient(new LawMinMaxTimeOfIngredient(IngredientType.EGGS, 3, 8));
        lawMinMaxService.addNewLawMinMaxTimeOfIngredient(new LawMinMaxTimeOfIngredient(IngredientType.FATS, 0, 25));
        lawMinMaxService.addNewLawMinMaxTimeOfIngredient(new LawMinMaxTimeOfIngredient(IngredientType.FISHES, 1, Integer.MAX_VALUE));
        lawMinMaxService.addNewLawMinMaxTimeOfIngredient(new LawMinMaxTimeOfIngredient(IngredientType.LEGUMES, 1, 3));
        lawMinMaxService.addNewLawMinMaxTimeOfIngredient(new LawMinMaxTimeOfIngredient(IngredientType.MEATS, 6, 10));
        lawMinMaxService.addNewLawMinMaxTimeOfIngredient(new LawMinMaxTimeOfIngredient(IngredientType.MILKS, 0, 4));
        lawMinMaxService.addNewLawMinMaxTimeOfIngredient(new LawMinMaxTimeOfIngredient(IngredientType.OIL_SEEDS, 2, Integer.MAX_VALUE));

    }

    private void setUpLawMinMaxPerDayPerAgeGroupTable(LawMinMaxPerDayPerAgeGroupService lawMinMaxAgeGroupService) {
        lawMinMaxAgeGroupService.addNewLawMinMaxPerDayPerAgeGroup(new LawMinMaxPerDayPerAgeGroup(AgeGroup.ONE_TO_THREE, 1100, 1300, 2, 5));
        lawMinMaxAgeGroupService.addNewLawMinMaxPerDayPerAgeGroup(new LawMinMaxPerDayPerAgeGroup(AgeGroup.FOUR_TO_SIX, 1350, 1650, 3, 7));
        lawMinMaxAgeGroupService.addNewLawMinMaxPerDayPerAgeGroup(new LawMinMaxPerDayPerAgeGroup(AgeGroup.SEVEN_TO_TEN, 1700, 2050, 5, 10));
        lawMinMaxAgeGroupService.addNewLawMinMaxPerDayPerAgeGroup(new LawMinMaxPerDayPerAgeGroup(AgeGroup.ELEVEN_TO_FOURTEEN, 2000, 2400, 5, 10));
        lawMinMaxAgeGroupService.addNewLawMinMaxPerDayPerAgeGroup(new LawMinMaxPerDayPerAgeGroup(AgeGroup.FIFTEEN_TO_EIGHTTEEN, 2000, 2600, 5, 12));
        lawMinMaxAgeGroupService.addNewLawMinMaxPerDayPerAgeGroup(new LawMinMaxPerDayPerAgeGroup(AgeGroup.NINETEEN_TO_SIXTYNINE, 2000, 2500, 5, 12));
        lawMinMaxAgeGroupService.addNewLawMinMaxPerDayPerAgeGroup(new LawMinMaxPerDayPerAgeGroup(AgeGroup.OVER_SIXTYNINE, 2000, 2400, 5, 12));
    }

    private void loadIngredientHundreadGramFromFile(String file, UserEntityService userService, IngredientHundredGramService ingredientService) {
        try {
            Scanner sc = new Scanner(new File(file), "utf-8");
            int rowCounter = 0;
            while (sc.hasNext()) {
                String[] row = sc.nextLine().split(";");
                rowCounter++;
                if (rowCounter > 1) {
                    Boolean rawIngredient = row[0].equalsIgnoreCase("TRUE");
                    IngredientType ingredientType;
                    switch (row[1]) {
                        case "ALCOHOLS":
                            ingredientType = IngredientType.ALCOHOLS;
                            break;
                        case "OIL_SEEDS":
                            ingredientType = IngredientType.OIL_SEEDS;
                            break;
                        case "OTHERS":
                            ingredientType = IngredientType.OTHERS;
                            break;
                        case "CEREALS":
                            ingredientType = IngredientType.CEREALS;
                            break;
                        case "FRUITS":
                            ingredientType = IngredientType.FRUITS;
                            break;
                        case "FISHES":
                            ingredientType = IngredientType.FISHES;
                            break;
                        case "MEATS":
                            ingredientType = IngredientType.MEATS;
                            break;
                        case "LEGUMES":
                            ingredientType = IngredientType.LEGUMES;
                            break;
                        case "MILKS":
                            ingredientType = IngredientType.MILKS;
                            break;
                        case "EGGS":
                            ingredientType = IngredientType.EGGS;
                            break;
                        case "VEGETABLES":
                            ingredientType = IngredientType.VEGETABLES;
                            break;
                        case "FATS":
                            ingredientType = IngredientType.FATS;
                            break;
                        default:
                            ingredientType = null;
                    }
                    String ingredientName = row[2];
                    double energyKJ = Double.parseDouble(row[3]);
                    double energyKcal = Double.parseDouble(row[4]);
                    double protein = Double.parseDouble(row[5]);
                    double fat = Double.parseDouble(row[6]);
                    double saturatedFat = Double.parseDouble(row[7]);
                    double carbohydrate = Double.parseDouble(row[8]);
                    double sugar = Double.parseDouble(row[9]);
                    int glycemicIndex = Integer.parseInt(row[10]);
                    double fibre = Double.parseDouble(row[11]);
                    double natrium = Double.parseDouble(row[12]);
                    double potassium = Double.parseDouble(row[13]);
                    double calcium = Double.parseDouble(row[14]);
                    double magnesium = Double.parseDouble(row[15]);
                    Boolean allergenMilk = row[16].equalsIgnoreCase("TRUE");
                    Boolean allergenGluten = row[17].equalsIgnoreCase("TRUE");
                    Boolean allergenEgg = row[18].equalsIgnoreCase("TRUE");
                    Boolean allergenShellfish = row[19].equalsIgnoreCase("TRUE");
                    Boolean allergenFish = row[20].equalsIgnoreCase("TRUE");
                    Boolean allergenMolluscs = row[21].equalsIgnoreCase("TRUE");
                    Boolean allergenPeanut = row[22].equalsIgnoreCase("TRUE");
                    Boolean allergenWalnut = row[23].equalsIgnoreCase("TRUE");
                    Boolean allergenSesame = row[24].equalsIgnoreCase("TRUE");
                    Boolean allergenSoy = row[25].equalsIgnoreCase("TRUE");
                    Boolean allergenCelery = row[26].equalsIgnoreCase("TRUE");
                    Boolean allergenMustard = row[27].equalsIgnoreCase("TRUE");
                    Boolean allergenSulpithes = row[28].equalsIgnoreCase("TRUE");
                    Boolean allergenLupin = row[29].equalsIgnoreCase("TRUE");
                    LocalDateTime lastModified = LocalDateTime.now();
                    UserEntity lastModifier = (UserEntity) userService.loadUserByUsername("admin@leedthefeed.hu");

                    IngredientHundredGram newInggredient = new IngredientHundredGram(rawIngredient, ingredientType, ingredientName, energyKJ, energyKcal,
                            protein, fat, saturatedFat, carbohydrate, sugar, glycemicIndex, fibre, natrium,
                            potassium, calcium, magnesium, allergenMilk, allergenGluten, allergenEgg, allergenShellfish,
                            allergenFish, allergenMolluscs, allergenPeanut, allergenWalnut, allergenSesame, allergenSoy,
                            allergenCelery, allergenMustard, allergenSulpithes, allergenLupin, lastModified, lastModifier);
                    ingredientService.addNewIngredient(newInggredient);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StartAppInitDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
