/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.config;

import com.google.gson.Gson;
import com.mycompany.leadthefeedproject.entities.AuthorityRole;
import com.mycompany.leadthefeedproject.entities.UserEntity;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

/**
 *
 * @author andras
 */
@Configuration
public class LoginSuccessHandler implements AuthenticationSuccessHandler{
    
    private Gson gson = new Gson();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<AuthorityRole> authorities = user.getRoles();
        List<String> roles = new ArrayList<>();
        for (AuthorityRole a : authorities) {
            roles.add(a.getRoleName());
        }
        String toJson = gson.toJson(roles);
        PrintWriter writer = response.getWriter();
        response.setStatus(200);
        response.setHeader("Acces-Control-Allow-Origin", "http://localhost:4200, localhost:4200, https://lead-the-feed.herokuapp.com");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        writer.print(toJson);
        writer.flush();
        
    }

    
   
}
