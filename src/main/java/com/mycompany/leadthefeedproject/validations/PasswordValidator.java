/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.validations;

import com.mycompany.leadthefeedproject.restcontroller.UserRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author andras
 */
@Component
public class PasswordValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

    public PasswordValidator() {
    }
    
    public boolean isValid(String password1, String password2){
        return password1.equals(password2);
    }
}
