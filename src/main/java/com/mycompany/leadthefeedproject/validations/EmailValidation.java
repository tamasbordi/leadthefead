/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.validations;

import com.mycompany.leadthefeedproject.restcontroller.UserRestController;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author andras
 */
@Component
public class EmailValidation {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRestController.class);

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(.[A-Za-z0-9]+)*(.[A-Za-z]{2,})$";

    private Pattern pattern;
    private Matcher matcher;

    public EmailValidation() {
    }

    public boolean validateEmail(String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        LOGGER.debug("Pattern.compile(EMAIL_PATTERN) result");
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
