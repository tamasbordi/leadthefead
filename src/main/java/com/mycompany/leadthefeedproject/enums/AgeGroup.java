/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.enums;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public enum AgeGroup {
    ONE_TO_THREE, FOUR_TO_SIX, SEVEN_TO_TEN, ELEVEN_TO_FOURTEEN, FIFTEEN_TO_EIGHTTEEN, NINETEEN_TO_SIXTYNINE, OVER_SIXTYNINE;
}
