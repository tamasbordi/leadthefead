/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.enums;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public enum MealTime {
    BREAKFAST, FORENOON_SNACK, LUNCH, AFTERNOON_SNACK, DINNER;
}
