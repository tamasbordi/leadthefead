/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.leadthefeedproject.enums;

/**
 *
 * @author Tamas Bordi <borditamas@gmail.com>
 */
public enum IngredientType {
    ALCOHOLS, OIL_SEEDS, OTHERS, CEREALS, FRUITS, FISHES, MEATS, LEGUMES, MILKS, EGGS, VEGETABLES, FATS;
}
